package org.bsmh.sortinghat.backend.Grades;

import javassist.NotFoundException;
import org.bsmh.sortinghat.backend.Grades.DTOS.GradesDTO;
import org.bsmh.sortinghat.backend.Grades.DTOS.GradesStatsDTO;
import org.bsmh.sortinghat.backend.Grades.DTOS.QuestionsDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping("/api/grades")
public class GradesController {


    @Autowired private GradesService service;


    @GetMapping("/test/{cycleId}/{testId}")
    public List<GradesDTO> getAllGrades(@PathVariable Long cycleId, @PathVariable Long testId) {
        return (service.getGrades(testId, cycleId));
    }

    @GetMapping("/test/split/{cycleId}/{testId}/{studentId}")
    public List<QuestionsDTO> getSplitGrades(@PathVariable Long cycleId, @PathVariable Long testId, @PathVariable String studentId) {
        try {
            return (service.getSplitGradePerStud(cycleId,testId, studentId));
        } catch (NotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "stats not found");
        }
    }

    @GetMapping("/test/split/{cycleId}/{testId}/{studentId}/{questionId}")
    public List<StudentsGrades> getCasesPerQuestionPerStudent(@PathVariable Long cycleId, @PathVariable Long testId, @PathVariable String studentId, @PathVariable Long questionId) {
        return (service.getCasesPerQuestionPerStudent(cycleId, testId, studentId, questionId));
    }

    @GetMapping("/stats/{cycleId}/{testId}")
    public HashMap<String, HashMap<Long, GradesStatsDTO>> getGrades(@PathVariable Long cycleId, @PathVariable Long testId) {
        try {
            return this.service.getAllUsersStats(cycleId, testId);
        } catch (NotFoundException e) {
           throw new ResponseStatusException(HttpStatus.NOT_FOUND, "stats not found");
        }
    }

}
