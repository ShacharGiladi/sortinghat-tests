package org.bsmh.sortinghat.backend.Users.DTOS;

import org.bsmh.sortinghat.backend.Users.Models.User;

public class FullUserInfoDTO {

    public String id;
    public String username;
    public String password;
    public int role;

    public FullUserInfoDTO(User user) {
        this.id = user.getId();
        this.username = user.getUsername();
        this.password = user.getPassword();
        this.role = user.getRole();
    }

    public FullUserInfoDTO() {
        this.id = "";
        this.username = "";
        this.password = "";
        this.role = 1;
    }
}
