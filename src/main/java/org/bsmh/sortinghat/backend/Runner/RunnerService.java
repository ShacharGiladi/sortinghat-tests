package org.bsmh.sortinghat.backend.Runner;

import org.bsmh.sortinghat.backend.Answers.StudentAnswer;
import org.bsmh.sortinghat.backend.Grades.GradesService;
import org.bsmh.sortinghat.backend.Loggers.RequestLogger;
import org.bsmh.sortinghat.backend.Questions.Models.Case;
import org.bsmh.sortinghat.backend.Questions.Models.Question;
import org.bsmh.sortinghat.backend.Questions.Repositorys.QuestionRepository;
import org.bsmh.sortinghat.backend.Runner.DTO.CheckResultDTO;
import org.bsmh.sortinghat.backend.Runner.DTO.CompilationResultDTO;
import org.bsmh.sortinghat.backend.Runner.DTO.UserResponseDTO;
import org.bsmh.sortinghat.backend.Runner.Exceptions.ErrorInProcessException;
import org.bsmh.sortinghat.backend.Runner.Exceptions.EvilCodeException;
import org.bsmh.sortinghat.backend.Runner.Exceptions.InfiniteLoopException;
import org.bsmh.sortinghat.backend.Runner.Exceptions.SyntaxErrorException;
import org.bsmh.sortinghat.backend.Runner.LanguagesRunner.CodeRunner;
import org.bsmh.sortinghat.backend.Runner.LanguagesRunner.CodeWithCompileRunner;
import org.bsmh.sortinghat.backend.Users.Services.UsersCodeService;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.Principal;
import java.util.*;

@Service
public class RunnerService {

    @Value("${upload.directory}")
    String uploadDir;

    @Autowired
    RunnerFactory runnerFactory;

    @Autowired
    QuestionRepository questionRepository;

    @Autowired
    GradesService gradesService;

    @Autowired
    UsersCodeService usersCodeService;

    private RequestLogger logger;

    public RunnerService() {
        this.logger = new RequestLogger(LoggerFactory.getLogger(this.getClass()));
    }

    public UserResponseDTO submit(RunnerRequestData data, Principal principal) throws Exception {
        String userId = principal.getName();
        Question question = questionRepository.findById(data.getQuestionId()).get();
        Path relativePath = createFile(data.getCode(), data.getFileExtension(), userId, data.getCycleId(), data.getTestId(), data.getQuestionId());
        Path filePath = relativePath.toAbsolutePath();
        CodeRunner codeRunner = runnerFactory.getRunner(data.getFileExtension());
        usersCodeService.submitCode(data.getCycleId(), data.getTestId(), principal.getName(),  data.getFileExtension(),  data.getCode(), data.getQuestionId());
        try {
            codeRunner.checkIfCodeIsEvil(data.getCode());
        } catch (EvilCodeException ex) {
            logger.error(principal, "this code was found as evil \n Evil word(s) found: " + ex.getErrorList());
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, ex.getMessage());
        }

        if (runnerFactory.isNeedToCompile(data.getFileExtension())) {
            try {
                logger.info(principal, "compile " + data.getFileExtension() + " code");
                filePath = Paths.get(((CodeWithCompileRunner) codeRunner).compileCode(filePath.toAbsolutePath().toString(), userId)).toAbsolutePath();
            } catch (SyntaxErrorException e) {
                logger.error(principal, "syntax error");
                StudentAnswer studentAnswer = new StudentAnswer();
                removeFileNameFromError(e.getErrorList(), filePath.toString(), data.getFileExtension());
                removeFileNameFromError(e.getErrorList(), relativePath.toString(), data.getFileExtension());
                System.out.println(relativePath.toString());
                return new UserResponseDTO(0, question.getCases().size(), new HashMap<>(), e.getErrorList());
            }
        }

        int numberOfSuccess = 0;
        List<String> errorList = new ArrayList<>();
        UserResponseDTO userResponseDTO = new UserResponseDTO();
        Integer caseCounter = 0;

        for (Case currentCase : question.getCases()) {
            try {
                caseCounter++;
                List<String> outputsFromStudent = codeRunner.run(filePath.toString(), currentCase.getCaseInputs(), userId);
                List<String> requireOutputs = currentCase.getOutputs();
                if (outputsFromStudent.equals(requireOutputs)) {
                    gradesService.saveStudentCase(userId, data.getCycleId(), data.getTestId(), question, currentCase, true, "");
                    numberOfSuccess++;
                } else {
                    gradesService.saveStudentCase(userId, data.getCycleId(), data.getTestId(), question, currentCase, false, "student's output : " + Arrays.toString(outputsFromStudent.toArray()) + " expected : " + Arrays.toString(currentCase.getOutputs().toArray()));
                }
            } catch (ErrorInProcessException ex) {
                logger.error(principal, "error in process");
                removeFileNameFromError(ex.getErrors(), filePath.toString(), data.getFileExtension());
                String error = String.join("\n", ex.getErrors());
                gradesService.saveStudentCase(userId, data.getCycleId(), data.getTestId(), question, currentCase, false, "student's error : " + error);
                addCaseToError(userResponseDTO.getErrors(), caseCounter, error);
            } catch (InfiniteLoopException ex) {
                logger.error(principal, "infinite loop error");
                gradesService.saveStudentCase(userId, data.getCycleId(), data.getTestId(), question, currentCase, false, "student had infinite loop");
            }
        }

        userResponseDTO.setNumOfCases(question.getCases().size());
        userResponseDTO.setNumOfSuccess(numberOfSuccess);

        return userResponseDTO;
    }

    public CheckResultDTO check(RunnerRequestData data, Principal principal) throws Exception {
        Path filePath = createFile(data.getCode(), data.getFileExtension(), "check_" + principal.getName(),
                data.getCycleId(), data.getTestId(), data.getQuestionId());

        CodeRunner runner = (runnerFactory.getRunner(data.getFileExtension()));
        String exePath = "";
        List<String> outputs;

        try {
            runner.checkIfCodeIsEvil(data.getCode());
        } catch (EvilCodeException ex) {
            logger.error(principal, "this code was found as evil \n" + data.getCode());
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, ex.getMessage());
        }


        try{
            if (runnerFactory.isNeedToCompile(data.getFileExtension())) {
                exePath = ((CodeWithCompileRunner)runner).compileCode(filePath.toAbsolutePath().toString(), principal.getName());

            } else {
                exePath = filePath.toAbsolutePath().toString();
            }

            outputs = runner.run(exePath, data.getInputs(), principal.getName());
        } catch (ErrorInProcessException ex) {
            logger.error(principal, "error in process");
            removeFileNameFromError(ex.getErrors(), exePath, data.getFileExtension());
            removeFileNameFromError(ex.getErrors(), filePath.toString(), data.getFileExtension());
            return new CheckResultDTO(ex);
        } catch (SyntaxErrorException ex) {
            logger.error(principal, "syntax error");
            removeFileNameFromError(ex.getErrorList(), filePath.toString(), data.getFileExtension());
            removeFileNameFromError(ex.getErrorList(), filePath.toAbsolutePath().toString(), data.getFileExtension());
            return new CheckResultDTO(ex);
        } catch(InfiniteLoopException ex) {
            logger.error(principal, "infinite loop");
            return  new CheckResultDTO(true);
        }

        return new CheckResultDTO(outputs);
    }

    public CompilationResultDTO compile(RunnerRequestData data, Principal principal) throws Exception {

        Path relativePath = createFile(data.getCode(), data.getFileExtension(), "check_" + principal.getName(),
                data.getCycleId(), data.getTestId(), data.getQuestionId());
        Path filePath = relativePath.toAbsolutePath();

        CodeRunner runner = (runnerFactory.getRunner(data.getFileExtension()));

        if (runnerFactory.isNeedToCompile(data.getFileExtension())) {
            try {
                ((CodeWithCompileRunner) runner).compileCode(filePath.toAbsolutePath().toString(), principal.getName());
            } catch (SyntaxErrorException e) {
                logger.error(principal, "syntax error");
                removeFileNameFromError(e.getErrorList(), filePath.toString(), data.getFileExtension());
                removeFileNameFromError(e.getErrorList(), relativePath.toString(), data.getFileExtension());
                return new CompilationResultDTO(e);
            }
        }

        return new CompilationResultDTO();
    }


    private void removeFileNameFromError(List<String> errors, String fileName, String fileExtension) {
        int counter = 0;
        for (String error : errors) {
            errors.set(counter, error.replace(fileName, "code." + fileExtension));
            counter++;
        }
    }

    private void addCaseToError(HashMap<String, List<String>> hashMap, Integer caseNumber, String error) {
        if (hashMap.containsKey(error)) {
            hashMap.get(error).add(caseNumber.toString());
        } else {
            ArrayList<String> cases = new ArrayList<>();
            cases.add(caseNumber.toString());
            hashMap.put(error, cases);
        }
    }


    private Path createFile(String code, String fileExtension, String userId, Long cycleId, Long testId, Long questionId) throws IOException {
        Path path = Paths.get(uploadDir, userId, cycleId.toString(), testId.toString(), questionId.toString(), userId + "." + fileExtension);
        Files.createDirectories(path.getParent());
        byte[] data = code.getBytes();
        Files.write(path, data);
        return path;
    }
}
