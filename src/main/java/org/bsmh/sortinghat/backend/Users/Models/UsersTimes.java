package org.bsmh.sortinghat.backend.Users.Models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.bsmh.sortinghat.backend.Tests.Test;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "user_tests_times")
public class UsersTimes {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "user_id")
    @JsonIgnore
    private String userId;

    @Column(name = "date_of_start")
    @Temporal(TemporalType.TIMESTAMP)
    private Date startDate;

    @Column(name = "date_of_end")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateOfEnd;

    @Column(name = "cycle_id")
    private Long cycleId;

    @JoinColumn(name = "test_id")
    @ManyToOne
    private Test test;

    public UsersTimes() {
    }

    public UsersTimes(String userId, Long testId, Long cycleId) {
        this.userId = userId;
        this.cycleId = cycleId;
        this.test = new Test();
        this.test.setId(testId);
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @JsonIgnore
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getDateOfEnd() {
        return dateOfEnd;
    }

    public void setDateOfEnd(Date dateOfEnd) {
        this.dateOfEnd = dateOfEnd;
    }

    @JsonIgnore
    public Long getCycleId() {
        return cycleId;
    }

    @JsonIgnore
    public void setCycleId(Long cycleId) {
        this.cycleId = cycleId;
    }

    public Test getTest() {
        return test;
    }
}

