package org.bsmh.sortinghat.backend.Answers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController("/api/answers")
public class AnswerController {
    @GetMapping("/all/{cycleId}/{testId}")
    public void all(@PathVariable Long cycleId, @PathVariable Long testId) {

    }
}
