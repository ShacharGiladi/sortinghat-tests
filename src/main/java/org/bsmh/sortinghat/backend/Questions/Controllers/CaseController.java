package org.bsmh.sortinghat.backend.Questions.Controllers;

import org.bsmh.sortinghat.backend.EntityController;
import org.bsmh.sortinghat.backend.Questions.Repositorys.CaseRepository;
import org.bsmh.sortinghat.backend.Questions.Models.Case;
import org.bsmh.sortinghat.backend.Questions.Models.InputToCase;
import org.bsmh.sortinghat.backend.Questions.Models.OutputToCase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/cases")
public class CaseController extends EntityController<Case, Long> {

    @Autowired
    private CaseRepository caseRepository;

    @PostMapping("/{caseId}/inputs")
    public void addInputsToCase(@PathVariable Long caseId, @RequestBody List<InputToCase> inputs) {
        Case caseToAdd = this.getRepository().findById(caseId).get();
        caseToAdd.addInputs(inputs);
        caseRepository.save(caseToAdd);
    }

    @PostMapping("/{caseId}/outputs")
    public void addOutputsToCase(@PathVariable Long caseId, @RequestBody List<OutputToCase> outputs) {
        Case caseToAdd = this.getRepository().findById(caseId).get();
        caseToAdd.addOutputs(outputs);
        caseRepository.save(caseToAdd);
    }

    @Override
    public JpaRepository<Case, Long> getRepository() {
        return this.caseRepository;
    }
}
