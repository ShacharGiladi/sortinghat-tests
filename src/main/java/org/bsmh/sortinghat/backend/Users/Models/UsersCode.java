package org.bsmh.sortinghat.backend.Users.Models;

import javax.persistence.*;

@Entity
@Table(name = "users_code_submit")
public class UsersCode {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "cycle_id")
    private long cycleId;

    @Column(name = "question_id")
    private long questionId;

    @Column(name = "student_id")
    private String studentId;

    @Column(name = "code")
    private String code;

    @Column(name = "test_id")
    private long testId;

    @Column(name = "language_of_code")
    private String languageOfCode;

    public UsersCode(long cycleId, String studentId, String code, long testId, String languageOfCode, long questionId) {
        this.cycleId = cycleId;
        this.studentId = studentId;
        this.code = code;
        this.testId = testId;
        this.languageOfCode = languageOfCode;
        this.questionId = questionId;
    }

    public UsersCode() {
    }

    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getCycleId() {
        return cycleId;
    }

    public void setCycleId(long cycleId) {
        this.cycleId = cycleId;
    }

    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public long getTestId() {
        return testId;
    }

    public void setTestId(Integer testId) {
        this.testId = testId;
    }

    public void setTestId(long testId) {
        this.testId = testId;
    }

    public String getLanguageOfCode() {
        return languageOfCode;
    }

    public void setLanguageOfCode(String languageOfCode) {
        this.languageOfCode = languageOfCode;
    }

    public long getQuestionId() {
        return questionId;
    }

    public void setQuestionId(long questionId) {
        this.questionId = questionId;
    }
}
