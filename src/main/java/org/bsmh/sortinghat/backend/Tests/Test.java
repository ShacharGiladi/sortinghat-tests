package org.bsmh.sortinghat.backend.Tests;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.bsmh.sortinghat.backend.Questions.Models.Question;
import org.bsmh.sortinghat.backend.Cycles.Cycle;
import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "test")
public class Test {
    @Id
    @Column(name = "test_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "test_name")
    private String name;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "questions_in_test",
            joinColumns = {@JoinColumn(name = "test_id")},
            inverseJoinColumns = {@JoinColumn(name = "question_id")})
    @OrderColumn(name = "order_of_question")
    @JsonIgnore
    private List<Question> questions;

    @ManyToMany(mappedBy = "testsInCycle")
    @JsonIgnore
    private List<Cycle> testsCycles;

    @Column(name= "instructions")
    private String instructions;

    @Column(name= "time")
    private int time;

    public String getInstructions() {
        return instructions;
    }

    public void setInstructions(String instructions) {
        this.instructions = instructions;
    }

    public List<Cycle> getTestsCycles() {
        return testsCycles;
    }

    public void setTestsCycles(List<Cycle> testsCycles) {
        this.testsCycles = testsCycles;
    }

    public List<Question> getQuestions() {
        return questions;
    }

    public void setQuestions(List<Question> questions) {
        this.questions = questions;
    }



    public void addQuestions(List<Question> questionsToAdd) {
        for (Question question: questionsToAdd) {
            this.getQuestions().add(question);
        }
    }

    public void addQuestion(Question questionToAdd) {
        this.getQuestions().add(questionToAdd);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }
}