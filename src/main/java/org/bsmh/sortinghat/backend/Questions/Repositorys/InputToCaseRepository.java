package org.bsmh.sortinghat.backend.Questions.Repositorys;

import org.bsmh.sortinghat.backend.Questions.Models.InputToCase;
import org.springframework.data.jpa.repository.JpaRepository;

public interface InputToCaseRepository extends JpaRepository<InputToCase, Long> {
}
