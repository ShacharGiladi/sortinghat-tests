package org.bsmh.sortinghat.backend.Grades;

import javassist.NotFoundException;
import org.bsmh.sortinghat.backend.Grades.DTOS.GradesDTO;
import org.bsmh.sortinghat.backend.Grades.DTOS.GradesStatsDTO;
import org.bsmh.sortinghat.backend.Grades.DTOS.QuestionsDTO;
import org.bsmh.sortinghat.backend.Grades.DTOS.SplitGradesDTO;
import org.bsmh.sortinghat.backend.Questions.Models.Case;
import org.bsmh.sortinghat.backend.Questions.Models.Question;
import org.bsmh.sortinghat.backend.Questions.Repositorys.QuestionRepository;
import org.bsmh.sortinghat.backend.Tests.TestRepository;
import org.bsmh.sortinghat.backend.Users.Models.User;
import org.bsmh.sortinghat.backend.Users.Models.UsersCode;
import org.bsmh.sortinghat.backend.Users.Models.UsersTimes;
import org.bsmh.sortinghat.backend.Users.Repositorys.UserCodeRepository;
import org.bsmh.sortinghat.backend.Users.Repositorys.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.*;


@Service
public class GradesService {

    @Autowired
    private QuestionRepository questionRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    UserCodeRepository userCodeRepository;

    @Autowired
    private GradesRepository gradesRepository;

    @Autowired
    private TestRepository testRepository;

    public List<GradesDTO> getGrades(Long testId, Long cycleId) {
        List<StudentsGrades> filteredList = gradesRepository.findByCycleTest(cycleId, testId);

        ArrayList<GradesDTO> gradesDTOS = new ArrayList<>();

        HashMap<String, Double> gradesPerStudent = new HashMap<>();
        if (filteredList.size() == 0) {
            return gradesDTOS;
        }
        User currUser = filteredList.get(0).getUser();
        Question currQuestion = filteredList.get(0).getQuestion();
        User last = null;
        Double questionGrade = 0D;
        Double testGrade = 0D;

        for (StudentsGrades student : filteredList) {
            if (currUser.getId().equals(student.getUser().getId())) {
                if (currQuestion.getId() == student.getQuestion().getId() &&
                        student.getIsCorrect()) {
                    questionGrade += student.getCase().getWeight();
                } else {
                    testGrade += 100 * questionGrade * currQuestion.getWeight(testId);
                    if (student.getIsCorrect()) {
                        questionGrade = student.getCase().getWeight();
                    } else {
                        questionGrade = 0D;
                    }
                    currQuestion = student.getQuestion();
                }
            } else {
                testGrade += 100 * questionGrade * currQuestion.getWeight(testId);
                //gradesPerStudent.put(studentId, testGrade);
                gradesDTOS.add(new GradesDTO(currUser.getId(), currUser.getUsername(), testGrade));
                currUser = student.getUser();
                testGrade = 0D;
                if (student.getIsCorrect()) {
                    questionGrade = student.getCase().getWeight();
                } else {
                    questionGrade = 0D;
                }
                currQuestion = student.getQuestion();
            }
        }

        testGrade += 100 * questionGrade * currQuestion.getWeight(testId);
        //gradesPerStudent.put(studentId, testGrade);
        gradesDTOS.add(new GradesDTO(currUser.getId(), currUser.getUsername(), testGrade));

        return (gradesDTOS);
    }



    public List<QuestionsDTO> getSplitGradePerStud(Long cycleId, Long testId, String studentId) throws NotFoundException {
        List<StudentsGrades> studentsGrades = gradesRepository.getUserAnswers(cycleId, testId, studentId);
        List<UsersCode> usersCodes = this.userCodeRepository.findByCycleIdAndTestIdAndStudentId(cycleId, testId, studentId);

        HashMap<String, HashMap<Long, GradesStatsDTO>> usersSubmition = this.getStats(cycleId, testId, getAnswersPerStudnts(studentsGrades), usersCodes);

        HashMap<Long, GradesStatsDTO> gradesStats = usersSubmition.get(studentId);
        List<Question> questions = testRepository.findById(testId).get().getQuestions();
        User user = userRepository.findById(studentId).get();

        Date dateOfSubmit = getUserTestTime(user, cycleId, testId);
        List<QuestionsDTO> gradesQuestions = new ArrayList<>();

        for(Question question : questions) {
            GradesStatsDTO currStats = gradesStats.get(question.getId());
            if(currStats == null) {
                GradesStatsDTO newStats = new GradesStatsDTO(question.getId(), "לא הגיש את השאלה", dateOfSubmit);
                gradesQuestions.add(new QuestionsDTO(newStats, question.getName()));
            } else {
                gradesQuestions.add(new QuestionsDTO(currStats, question.getName()));
            }
        }

        return gradesQuestions;
    }

    public HashMap<String, HashMap<Long, GradesStatsDTO>> getAllUsersStats(Long cycleId, Long testId) throws NotFoundException {
        List<UsersCode> allCode = this.userCodeRepository.findByCycleIdAndTestId(cycleId, testId);
        HashMap<String, List<StudentsGrades>> allGrades =  this.getAnswersPerStudnts(this.gradesRepository.findByCycleIdAndTestIdOrderByUser(cycleId, testId));
        return getStats(cycleId, testId,allGrades, allCode);
    }


    public HashMap<String, List<StudentsGrades>> getAnswersPerStudnts( List<StudentsGrades> studentsGrades) {
        HashMap<String, List<StudentsGrades>> usersQuestions = new HashMap<>();
        for (StudentsGrades currStudntGrades : studentsGrades) {
            if (usersQuestions.get(currStudntGrades.getUser().getId()) == null) {
                usersQuestions.put(currStudntGrades.getUser().getId(), new ArrayList<>());
            }

            usersQuestions.get(currStudntGrades.getUser().getId()).add(currStudntGrades);
        }

        return usersQuestions;
    }




    public HashMap<String, HashMap<Long, GradesStatsDTO>> getStats(Long cycleId, Long testId,  HashMap<String, List<StudentsGrades>> codeSubmissions, List<UsersCode> usersCodes) throws NotFoundException {
        HashMap<String, HashMap<Long, GradesStatsDTO>> stats = new HashMap<>();
        for (String currStudent : codeSubmissions.keySet()) {
            for (StudentsGrades currGrade : codeSubmissions.get(currStudent)) {
                if (stats.get(currStudent) == null) {
                    stats.put(currStudent, new HashMap<>());
                }
                if (stats.get(currStudent).get(currGrade.getQuestion().getId()) == null) {
                    String languseOfCode = this.getLanguseOfStudnt(currStudent,cycleId, testId,currGrade.getQuestion().getId(), usersCodes);
                    stats.get(currStudent).put(currGrade.getQuestion().getId(), new GradesStatsDTO(currGrade.getQuestion().getId(), languseOfCode, this.getUserTestTime(currGrade.getUser(), cycleId, testId)));
                }

                if (currGrade.getIsCorrect()) {
                    stats.get(currStudent).get(currGrade.getQuestion().getId()).increseNumberOfSucess();
                }

                stats.get(currStudent).get(currGrade.getQuestion().getId()).increseNumberOfTotal();
            }
        }


        return stats;
    }

    private Date getUserTestTime(User user, Long cycleId, Long testId) {
        for(UsersTimes usersTimes : user.getUsersTimes()) {
            if(usersTimes.getCycleId() == cycleId && usersTimes.getTest().getId() == testId) {
                return usersTimes.getStartDate();
            }
        }

        return null;
    }

    private String getLanguseOfStudnt(String studntId, Long cycleId, Long testId, Long questionId, List<UsersCode> usersCodes) throws NotFoundException {
        Optional<UsersCode> usersCode = usersCodes.stream().filter(currCode -> currCode.getStudentId().equals(studntId) && currCode.getCycleId() == cycleId && currCode.getTestId() == testId && currCode.getQuestionId() == questionId).findFirst();
        if (usersCode.isPresent()) {
            return usersCode.get().getLanguageOfCode();
        }

        throw new NotFoundException("users code not found");
    }

    public List<StudentsGrades> getCasesPerQuestionPerStudent(Long cycleId, Long testId, String studentId, Long questionId) {
        return gradesRepository.getCases(cycleId, testId, questionId, studentId);
    }

    public void saveStudentCase(String userId, Long cycleId, Long testId, Question question, Case questionCase, Boolean isCorrect, String description) {
        StudentsGrades newGrade = new StudentsGrades(userRepository.findById(userId).get(), cycleId, questionCase, question, testId, isCorrect, description, question.getWeight(testId));
        Optional<StudentsGrades> studentsGrade = gradesRepository.getUserAnswer(cycleId, testId, questionCase.getCaseId(), userId);

        if (studentsGrade.isPresent()) {
            newGrade.setId(studentsGrade.get().getId());
        }

        gradesRepository.save(newGrade);
    }
}
