package org.bsmh.sortinghat.backend.Runner.LanguagesRunner;

import org.bsmh.sortinghat.backend.Runner.Exceptions.EvilCodeException;

import org.springframework.beans.factory.annotation.Value;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

public class CppRunner extends CodeWithCompileRunner {
    @Value("${runner.cpp.compiler}")
    private String cppCompiler;

    @Override
    public List<String> run(String fileLocation, List<String> processInputs, String userId) throws Exception {
        this.setCompilerCommand(cppCompiler);
        List<String> outputs =  super.run(fileLocation, processInputs, userId);
        deleteObjFile(fileLocation);
        return outputs;
    }

    private void deleteObjFile(String fileLocation) {
        Path objFileLocation = Paths.get(new File(fileLocation).getAbsolutePath());

        // Change the extension to the obj file
        File objFile = new File(super.changeFileExtension(objFileLocation.toAbsolutePath().toString(), "obj"));
        objFile.delete();
    }

    @Override
    public String compileCode(String fileLocation, String userId) throws Exception {
        this.setCompilerCommand(cppCompiler);
        String compileOutput = super.compileCode(fileLocation, userId);
        deleteObjFile(fileLocation);
        return compileOutput;
    }

    @Override
    public void checkIfCodeIsEvil(String code) throws  EvilCodeException {
        String[] keywords = new String[] {"base64","system", "socket", "HANDLE",
                "FileSystem", "unistd", "Windows"};

        super.checkIfCodeIsEvil(code, Arrays.asList(keywords));
    }
}
