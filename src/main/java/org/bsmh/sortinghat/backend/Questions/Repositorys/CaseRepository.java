package org.bsmh.sortinghat.backend.Questions.Repositorys;

import org.bsmh.sortinghat.backend.Questions.Models.Case;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CaseRepository extends JpaRepository<Case, Long> {}
