package org.bsmh.sortinghat.backend.Questions.Controllers;

import org.bsmh.sortinghat.backend.EntityController;
import org.bsmh.sortinghat.backend.Questions.DTO.QuestionManagementDTO;
import org.bsmh.sortinghat.backend.Questions.Models.Case;
import org.bsmh.sortinghat.backend.Questions.Models.Question;
import org.bsmh.sortinghat.backend.Questions.QuestionService;
import org.bsmh.sortinghat.backend.Questions.Repositorys.QuestionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/questions")
public class QuestionController extends EntityController<Question, Long> {

    @Autowired
    private QuestionRepository questionRepository;

    @Autowired
    private QuestionService questionService;

    @GetMapping("/all")
    public List<QuestionManagementDTO> getQuestions(Principal principal) {
        this.logger.debug(principal, "get all of the questions in all the tests");
        return questionService.getQuestions();
    }

    @GetMapping("/find/{questionId}")
    public QuestionManagementDTO getQuestionById(@PathVariable Long questionId, Principal principal) {
        this.logger.debug(principal, "get question: "+questionId);
        return this.questionService.getQuestionById(questionId);
    }

    @GetMapping("/{questionId}/cases")
    public List<Case> getAllCasesInQuestion(@PathVariable Long questionId, Principal principal) {
        this.logger.debug(principal, "get all of the cases in question for question=" + questionId);
        return this.questionService.getAllCasesInQuestion(questionId);
    }

    @PostMapping("/question")
    public void addQuestion(@RequestBody Question questionToAdd, Principal principal) {
        this.questionService.addQuestion(questionToAdd);
    }

    @PutMapping("/{questionId}/cases")
    public void addCases(@PathVariable Long questionId, @RequestBody List<Case> cases, Principal principal) {
       // option to add multiple cases at once
    }

    @PostMapping("/{questionId}/case")
    public void addCaseToQuestion(@PathVariable Long questionId, @RequestBody Case caseToAdd, Principal principal) {
        this.logger.debug(principal,"add case to question for question= " + questionId);
        this.questionService.addCaseToQuestion(questionId,caseToAdd);
    }

    @DeleteMapping("/{questionId}/case/{caseToDeleteId}")
    public void deleteCaseOfQuestion(@PathVariable Long questionId, @PathVariable Long caseToDeleteId, Principal principal) {
        this.logger.debug(principal,"deleting case of question= " + questionId);
        this.questionService.deleteCaseOfQuestion(questionId,caseToDeleteId);
    }

    @PostMapping("/{questionId}/cases")
    public void deleteCasesOfQuestion(@PathVariable Long questionId, @RequestBody Long[] casesToDeleteId, Principal principal) {
        this.logger.debug(principal,"deleting case of question= " + questionId);
        this.questionService.deleteCasesOfQuestion(questionId,casesToDeleteId);
    }

    @PutMapping("/description")
    public void changeDescription(@RequestBody Question newQuestion, Principal principal) {
        this.logger.debug(principal,"change description to question for question= " + newQuestion.getId());
        this.questionService.changeDescription(newQuestion);
    }

    @Override
    public JpaRepository<Question, Long> getRepository() {
        return this.questionRepository;
    }
}
