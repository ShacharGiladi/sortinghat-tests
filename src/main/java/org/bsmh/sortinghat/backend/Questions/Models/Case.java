package org.bsmh.sortinghat.backend.Questions.Models;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Entity
@Table(name = "cases")
public class Case {
    @Id
    @Column(name = "case_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long caseId;

    @Column(name = "weight")
    private double weight;

    @Column(name = "question_id")
    private Long questionId;

    @Column(name = "disabled")
    private boolean disabled;

    @OneToMany
    @JoinColumn(name = "case_id")
    @OrderBy("order_of_input")
    private List<InputToCase> caseInputs = new ArrayList<>();

    @OneToMany
    @JoinColumn(name = "case_id")
    @OrderBy("order_of_output")
    private List<OutputToCase> outputs = new ArrayList<>();

    public void addInputs(List<InputToCase> inputs) {
        this.caseInputs.addAll(inputs);
    }

    public void addOutputs(List<OutputToCase> outputsToAdd) {
        this.outputs.addAll(outputsToAdd);
    }

    public Long getCaseId() {
        return caseId;
    }

    public void setCaseId(Long caseId) {
        this.caseId = caseId;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public Long getQuestionId() {
        return questionId;
    }

    public void setQuestionId(Long questionId) {
        this.questionId = questionId;
    }

    public boolean isDisabled() {
        return disabled;
    }

    public void setDisabled(boolean disabled) {
        this.disabled = disabled;
    }

    public List<String> getCaseInputs() {
        return caseInputs.stream().map(current -> current.getInput()).collect(Collectors.toList());
    }

    public List<InputToCase> getCaseInputsAsObject() {
        return this.caseInputs;
    }

    public void setCaseInputs(List<InputToCase> caseInputs) {
        this.caseInputs = caseInputs;
    }

    public List<String> getOutputs() {
        return outputs.stream().map(curr -> curr.getOutput()).collect(Collectors.toList());
    }

    public List<OutputToCase> getCaseOutputsAsObject() {
        return this.outputs;
    }

    public void setOutputs(List<OutputToCase> outputs) {
        this.outputs = outputs;
    }

    @Override
    public String toString() {
        return "Case{" +
                "caseId=" + caseId +
                ", weight=" + weight +
                '}';
    }
}
