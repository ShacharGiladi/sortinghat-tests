package org.bsmh.sortinghat.backend.costumAssertions;

import org.assertj.core.api.AbstractAssert;
import org.bsmh.sortinghat.backend.Runner.RunnerRequestData;

public class RunnerRequestDataAssert extends AbstractAssert<RunnerRequestDataAssert, RunnerRequestData> {
    public RunnerRequestDataAssert(RunnerRequestData runnerRequestData) {
        super(runnerRequestData, RunnerRequestDataAssert.class);
    }

    public static RunnerRequestDataAssert assertThat(RunnerRequestData actual) {
        return new RunnerRequestDataAssert(actual);
    }

    public RunnerRequestDataAssert allEquals(RunnerRequestData expected) {
        if (!(expected.getFileExtension().equals(actual.getFileExtension()) &&
                expected.getTestId().equals(actual.getTestId()) &&
                expected.getQuestionId().equals(actual.getQuestionId()) &&
                expected.getInputs().equals(actual.getInputs()) &&
                expected.getCode().equals(actual.getCode()))) {
            failWithMessage("Expected and actual does not match\nActual: " + actual.toString() + "\nExpected: " + expected.toString());

        }
        return this;
    }

}
