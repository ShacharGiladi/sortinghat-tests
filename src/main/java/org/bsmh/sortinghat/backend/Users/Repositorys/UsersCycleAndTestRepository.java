package org.bsmh.sortinghat.backend.Users.Repositorys;

import org.bsmh.sortinghat.backend.Users.Models.UsersInCycleAndTest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UsersCycleAndTestRepository extends JpaRepository<UsersInCycleAndTest, Long> {
    List<UsersInCycleAndTest> findByUser_Id(String userId);
}
