package org.bsmh.sortinghat.backend.Users.Repositorys;

import org.bsmh.sortinghat.backend.Users.Models.User;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface UserRepository extends JpaRepository<User, String> {
     Optional<User> findByIdAndPassword(String id, String password);

     List<User> findByRoleLessThanEqual(int role);

     List<User> findByUsersTimes_DateOfEndIsAfterAndUsersTimes_DateOfEndIsBeforeAndUsersTimes_cycleIdEquals(Date startDate, Date endDate, Long cycleId);


     default List<User> findUsersOfToday(Long cycleId) {
          Date startDateOfToday = new Date();
          startDateOfToday.setHours(0);
          startDateOfToday.setMinutes(0);
          startDateOfToday.setSeconds(0);
          Date endDateOfToday = new Date();
          endDateOfToday.setHours(23);
          endDateOfToday.setMinutes(59);
          endDateOfToday.setSeconds(59);
          return findByUsersTimes_DateOfEndIsAfterAndUsersTimes_DateOfEndIsBeforeAndUsersTimes_cycleIdEquals(startDateOfToday, endDateOfToday, cycleId);
     }
}
