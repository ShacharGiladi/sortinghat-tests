package org.bsmh.sortinghat.backend.Grades;

import com.fasterxml.jackson.annotation.*;
import org.bsmh.sortinghat.backend.Questions.Models.Case;
import org.bsmh.sortinghat.backend.Questions.Models.Question;
import org.bsmh.sortinghat.backend.Users.Models.User;

import javax.persistence.*;

@Entity
@Table(name="answers_per_student_in_cycle")
public class StudentsGrades {

    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonIgnore
    private Long id;

    @ManyToOne
    @JoinColumn(name = "student_id")
    @JsonIdentityInfo(generator= ObjectIdGenerators.PropertyGenerator.class, property="id")
    @JsonIdentityReference(alwaysAsId=true)
    private User user;

    @Column(name="cycle_id")
    private Long cycleId;

    @ManyToOne
    @JoinColumn(name="case_id")
    private Case questionCase;

    @ManyToOne
    @JsonIdentityInfo(generator= ObjectIdGenerators.PropertyGenerator.class, property="id")
    @JsonIdentityReference(alwaysAsId=true)
    @JoinColumn(name="question_id")
    private Question question;

    @Column(name="test_id")
    private Long testId;

    @Column(name="is_correct")
    private boolean isCorrect;

    @Column(name="description")
    private String description;

    @JsonIgnore
    @Column(name = "weight_of_question")
    private Double weight;

    public StudentsGrades() { }

    public StudentsGrades(User user, Long cycleId, Case questionCase, Question question, Long testId, boolean isCorrect, String description, Double weight) {
        this.user = user;
        this.cycleId = cycleId;
        this.questionCase = questionCase;
        this.question = question;
        this.testId = testId;
        this.isCorrect = isCorrect;
        this.description = description;
        this.weight = weight;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Long getCycleId() {
        return cycleId;
    }

    public void setCycleId(Long cycleId) {
        this.cycleId = cycleId;
    }

    @JsonIdentityInfo(generator= ObjectIdGenerators.PropertyGenerator.class, property="caseId")
    @JsonIdentityReference(alwaysAsId=true)
    public Case getCase() {
        return questionCase;
    }

    public void setCase(Case questionCase) {
        this.questionCase = questionCase;
    }

    public Question getQuestion() {
        return question;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }

    public Long getTestId() {
        return testId;
    }

    public void setTestId(Long testId) {
        this.testId = testId;
    }

    public boolean getIsCorrect() {
        return isCorrect;
    }

    public void setIsCorrect(boolean correct) {
        isCorrect = correct;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }
}
