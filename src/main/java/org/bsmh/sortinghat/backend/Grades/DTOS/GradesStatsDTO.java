package org.bsmh.sortinghat.backend.Grades.DTOS;

import java.util.Date;

public class GradesStatsDTO {
    private Long questionId;
    private int correctCases;
    private int totalCases;
    private String languageCode;
    private Date dateOfSubmit;

    public GradesStatsDTO(Long questionId, String languageCode, Date dateOfSubmit) {
        this.questionId = questionId;
        this.totalCases = 0;
        this.correctCases = 0;
        this.languageCode = languageCode;
        this.dateOfSubmit = dateOfSubmit;
    }

    public void increseNumberOfSucess() {
        this.correctCases++;
    }

    public void increseNumberOfTotal() {
        this.totalCases++;
    }

    public int getCorrectCases() {
        return correctCases;
    }

    public int getTotalCases() {
        return totalCases;
    }

    public String getLanguageCode() {
        return languageCode;
    }

    public Date getDateOfSubmit() {
        return dateOfSubmit;
    }

    public Long getQuestionId() {
        return questionId;
    }

    public void setQuestionId(Long questionId) {
        this.questionId = questionId;
    }
}
