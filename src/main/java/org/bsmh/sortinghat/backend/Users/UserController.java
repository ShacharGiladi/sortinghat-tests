package org.bsmh.sortinghat.backend.Users;

import javassist.NotFoundException;
import org.bsmh.sortinghat.backend.Cycles.Cycle;
import org.bsmh.sortinghat.backend.EntityController;
import org.bsmh.sortinghat.backend.Grades.GradesRepository;
import org.bsmh.sortinghat.backend.Grades.StudentsGrades;
import org.bsmh.sortinghat.backend.Questions.Models.Case;
import org.bsmh.sortinghat.backend.Questions.QuestionService;
import org.bsmh.sortinghat.backend.Security.AuthExecptions.UnauthorizedException;
import org.bsmh.sortinghat.backend.Tests.Test;
import org.bsmh.sortinghat.backend.Users.DTOS.FullUserInfoDTO;
import org.bsmh.sortinghat.backend.Users.DTOS.StudentCasesDTO;
import org.bsmh.sortinghat.backend.Users.DTOS.UsersInCycleInTestDTO;
import org.bsmh.sortinghat.backend.Users.Exceptions.*;
import org.bsmh.sortinghat.backend.Users.Models.User;
import org.bsmh.sortinghat.backend.Users.Models.UsersCode;
import org.bsmh.sortinghat.backend.Users.Models.UsersInCycleAndTest;
import org.bsmh.sortinghat.backend.Users.Models.UsersTimes;
import org.bsmh.sortinghat.backend.Users.Repositorys.UserCodeRepository;
import org.bsmh.sortinghat.backend.Users.Repositorys.UserRepository;
import org.bsmh.sortinghat.backend.Users.Repositorys.UsersTimesRepository;
import org.bsmh.sortinghat.backend.Users.Services.UserService;
import org.bsmh.sortinghat.backend.Users.Services.UsersCodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.bsmh.sortinghat.backend.Cycles.CycleRepository;

import java.io.ByteArrayInputStream;
import java.security.Principal;

import java.util.*;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/users")
public class UserController extends EntityController<User, String> {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private UserService userService;
    @Autowired
    private GradesRepository gradesRepository;

    @Autowired
    private UsersCodeService usersCodeService;

    @Autowired
    private QuestionService questionService;

    @Autowired
    private UsersTimesRepository usersTimesRepository;
    @Autowired
    private UserCodeRepository userCodeRepository;

    @PutMapping("/{userId}/addTime/{minuetsToAdd}/{testId}/{cycleId}")
    @PreAuthorize("hasAnyAuthority('2', '3', '4')")
    public void addTimeToUser(@PathVariable String userId, @PathVariable Integer minuetsToAdd, @PathVariable Long testId, @PathVariable Long cycleId) throws UserDidntStartTestException {
        userService.addTimeToUser(minuetsToAdd, userId, testId, cycleId);
    }

    @PutMapping("{userId}/break/{testId}/{cycleId}")
    @PreAuthorize("hasAnyAuthority('2', '3', '4')")
    public User giveUserBreak(@PathVariable String userId, @PathVariable Long testId, @PathVariable Long cycleId) throws UserWasNotFoundException, UserIsAllReadyOnBreakException, UserDidntStartTestException {
        return userService.giveUserBreak(userId, testId, cycleId);
    }

    @PutMapping("{userId}/return/{cycleId}/{testId}")
    @PreAuthorize("hasAnyAuthority('2', '3', '4')")
    public User returnFromBreak(@PathVariable String userId, @PathVariable Long cycleId, @PathVariable Long testId) throws UserWasNotFoundException, UserIsNotInBreakException, UserDidntStartTestException {
        return userService.returnUserFromBreak(userId, cycleId, testId);
    }

    /**
     * Get the user time left by id (for guardians)
     *
     * @param id the id of the user
     * @return optional user time
     */
    @PreAuthorize("hasAnyAuthority('2', '3', '4')")
    @GetMapping("/time/{id}/{cycleId}/{testId}")
    public Optional<UsersTimes> getUserTimeByUserId(@PathVariable String id, @PathVariable Long cycleId, @PathVariable Long testId) {
        return usersTimesRepository.findByUserIdAndCycleIdAndTestId(id, cycleId, testId);
    }

    @PutMapping("/start/{testId}/{cycleId}")
    @PreAuthorize("hasAnyAuthority('1')")
    public void startTest(Principal principal, @PathVariable Long testId, @PathVariable Long cycleId) {
        try {
            this.userService.startUserTime(principal.getName(), testId, cycleId);
        } catch (DoubleStartException e) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Double Start");
        }
    }

    /**
     * Get the loged in user time (for students)
     *
     * @param principal the user loged
     * @return optional user time
     */
    @GetMapping("/time/{cycleId}/{testId}")
    public HashMap<String, Object> getLoggedUserTime(Principal principal, @PathVariable Long cycleId, @PathVariable Long testId) {
        HashMap<String, Object> hashMap = new HashMap<>();
        Optional<UsersTimes> usersTimes = usersTimesRepository.findByUserIdAndCycleIdAndTestId(principal.getName(), cycleId, testId);
        if (usersTimes.isPresent()) {
            hashMap.put("time", usersTimes.get());
        }

        User user = userRepository.findById(principal.getName()).get();
        hashMap.put("break", user.getIsUserInBreak());
        return hashMap;
    }


    // Check calls for /today/{cycleId}/{testId}
    @PreAuthorize("hasAnyAuthority('2', '3', '4')")
    @GetMapping("/today/{cycleId}")
    public List<User> getTodayUsers(@PathVariable Long cycleId) {
        return userRepository.findUsersOfToday(cycleId);
    }

    @Override
    public List<User> queryAll(Principal principal) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String auto = authentication.getAuthorities().iterator().next().toString();
        this.logger.info(principal, " auto  : " + auto);
        return userRepository.findByRoleLessThanEqual(Integer.parseInt(auto));
    }

    @GetMapping("/{id}/cycles")
    public Set<Cycle> getAllCycleByUser(@PathVariable String id, Principal principal) {
        this.logger.info(principal, "requested all cycles that user of user " + id);
        return userRepository.getOne(id).getStudentsCycles();

    }

    @GetMapping("/tests")
    public List<Test> getAllTestByUser(Principal principal) {
        this.logger.info(principal, "requested all tests that user of user " + principal.getName());
        return userRepository.getOne(principal.getName()).getUsersTests();
    }

    @GetMapping("/testincycle")
    public List<UsersInCycleAndTest> getUsersTestsAndCycles(Principal principal) {
        return userService.getUsersCyclesAndTests(principal.getName());
    }

    @GetMapping("/testincycle/{usersId}")
    public List<UsersInCycleAndTest> getUsersTestsAndCyclesById(@PathVariable String usersId) {
        return userService.getUsersCyclesAndTests(usersId);
    }

    @PreAuthorize("hasAnyAuthority('2', '3', '4')")
    @PostMapping("testincycle")
    @ResponseStatus(HttpStatus.CREATED)
    public void createTestInCylce(@RequestBody UsersInCycleAndTest testInCycle) {
        userService.createNewUsersCycleAndTest(testInCycle);
    }

    @PreAuthorize("hasAnyAuthority('2', '3', '4')")
    @DeleteMapping("testincycle/{testInCycleId}")
    public void deleteTestInCycle(@PathVariable Long testInCycleId) {
        userService.deleteUsersCycleAndTest(testInCycleId);
    }

    @PreAuthorize("hasAnyAuthority('2', '3', '4')")
    @PutMapping("/{id}/interview")
    public void changeStatusOfInterview(@PathVariable String id) {
        User user = this.getRepository().findById(id).get();
        user.setFinishedInterview(!user.getFinishedInterview());
        this.getRepository().save(user);
    }

    @PreAuthorize("hasAnyAuthority('2', '3', '4')")
    @GetMapping("/usersCycleTests")
    public List<UsersInCycleInTestDTO> allTestsAndCycles() {
        return this.userService.getAllTestsAndCycle();
    }

    @PreAuthorize("hasAnyAuthority('2', '3', '4')")
    @GetMapping("/all/fullInfo")
    public List<FullUserInfoDTO> getAllUsersData() {
        return this.userService.getAllUsersFullInfo();
    }

    @PostMapping("/create")
    public void createByList(@RequestBody List<User> list, Principal principal) {
        try {
            userService.createUsers(list);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "null inside user");
        }
    }

    @PostMapping("/create/{cycleId}/{testId}")
    public List<User> createMalshbs(@RequestBody List<User> list, Principal principal, @PathVariable Long cycleId, @PathVariable Long testId) {
        try {
            return userService.addUsersToCycle(cycleId, testId, list);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "null inside user");
        }
    }

    @PreAuthorize("hasAnyAuthority('2', '3', '4')")
    @PutMapping("/addToCycleAndTest/{cycleId}/{testId}")
    public void addMalshbsToCycleAndTest(@RequestBody List<User> list, Principal principal, @PathVariable Long cycleId, @PathVariable Long testId) {
        userService.addToCycleIfNotInCycle(cycleId, testId, list);
    }


    @GetMapping("/code/{cycleId}/{testId}")
    public List<UsersCode> getStudentCodeInTest(@PathVariable Long cycleId, @PathVariable Long testId, Principal principal) {
        return userCodeRepository.findByCycleIdAndTestIdAndStudentId(cycleId, testId, principal.getName());
    }

    @PostMapping("/login")
    public HashMap<String, Object> login(@RequestBody User user) {
        try {
            HashMap<String, Object> hashMap = new HashMap<>();
            hashMap.put("token", this.userService.signIn(user.getId(), user.getPassword()));
            hashMap.put("role", userRepository.findById(user.getId()).get().getRole());
            return hashMap;
        } catch (UnauthorizedException ex) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "User password or id is not correct");
        }
    }

    @GetMapping("/cases/{cycleId}/{testId}")
    public List<StudentCasesDTO> getCases(Principal principal, @PathVariable Long cycleId, @PathVariable Long testId) {
        List<StudentsGrades> studentsGrades = gradesRepository.getUserAnswers(cycleId, testId, principal.getName());

        return studentsGrades.stream().filter(currStudentGrade -> !currStudentGrade.getCase().isDisabled()).map(c-> new StudentCasesDTO(c)).collect(Collectors.toList());
    }

    @PreAuthorize("hasAnyAuthority('2', '3', '4')")
    @GetMapping("/{userId}/cases/{cycleId}/{testId}")
    public List<StudentsGrades> getCasesForUser(@PathVariable String userId, @PathVariable Long cycleId, @PathVariable Long testId) {
        List<StudentsGrades> studentsGrades = gradesRepository.getUserAnswers(cycleId, testId, userId);
        return studentsGrades;
    }

    @PreAuthorize("hasAnyAuthority('2', '3', '4')")
    @GetMapping("/code/{userId}/{cycleId}/{testId}/{questionId}")
    public UsersCode downloadFile (@PathVariable String userId, @PathVariable Long testId, @PathVariable Long questionId, @PathVariable Long cycleId) {
        try {
            return usersCodeService.getUserCode(userId, cycleId, testId, questionId);
        } catch(NotFoundException ex) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "User didnt submit the code");
        }

    }

    @PreAuthorize("hasAnyAuthority('2', '3', '4')")
    @GetMapping("/fullinfo/{userId}")
    public FullUserInfoDTO getFullUserInfo(@PathVariable String userId) {
        return userService.getFullUserInfo(userId);
    }

    @PreAuthorize("hasAnyAuthority('2', '3', '4')")
    @PutMapping("/edit")
    public void editUserInfo(@RequestBody FullUserInfoDTO fullUserInfo) {
        userService.editUser(fullUserInfo);
    }

    @Override
    public JpaRepository<User, String> getRepository() {
        return this.userRepository;
    }
}
