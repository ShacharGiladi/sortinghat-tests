package org.bsmh.sortinghat.backend.Configuration;


import org.bsmh.sortinghat.backend.Runner.LanguagesRunner.CppRunner;
import org.bsmh.sortinghat.backend.Runner.LanguagesRunner.CsRunner;
import org.bsmh.sortinghat.backend.Runner.LanguagesRunner.JavaRunner;
import org.bsmh.sortinghat.backend.Runner.LanguagesRunner.PythonRunner;
import org.bsmh.sortinghat.backend.Runner.RunnerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;

@org.springframework.context.annotation.Configuration
public class RunnerConfiguration {

    @Bean
    public RunnerFactory runnerFactory() {return new RunnerFactory();}

    @Bean
    @Scope("prototype")
    public PythonRunner pythonRunner() {
        return new PythonRunner();
    }

    @Bean
    @Scope("prototype")
    public JavaRunner javaRunner() {
        return new JavaRunner();
    }

    @Bean
    @Scope("prototype")
    public CsRunner csRunner() {
        return new CsRunner();
    }

    @Bean
    @Scope("prototype")
    public CppRunner cppRunner() {
        return new CppRunner();
    }
}
