package org.bsmh.sortinghat.backend.Users.Models;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "users_break")
public class UsersBreaks {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "user_id")
    private String userId;

    @Column(name = "date_of_exit")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateOfExit;

    @Column(name = "date_of_return")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateOfEnd;

    @Column(name = "did_return")
    private Boolean didReturn;

    @Column(name = "cycle_id")
    private Long cycleId;

    @Column(name = "test_id")
    private Long testId;

    public UsersBreaks() {
    }

    public UsersBreaks(String userId, Long testId, Long cycleId) {
        this.dateOfExit = new Date();
        this.didReturn = false;
        this.userId = userId;
        this.cycleId = cycleId;
        this.testId = testId;
    }


    public Date getDateOfExit() {
        return dateOfExit;
    }

    public void setDateOfExit(Date dateOfExit) {
        this.dateOfExit = dateOfExit;
    }

    public Date getDateOfEnd() {
        return dateOfEnd;
    }

    public void setDateOfEnd(Date dateOfEnd) {
        this.dateOfEnd = dateOfEnd;
    }

    public Boolean getDidReturn() {
        return didReturn;
    }

    public void setDidReturn(Boolean didReturn) {
        this.didReturn = didReturn;
    }
}
