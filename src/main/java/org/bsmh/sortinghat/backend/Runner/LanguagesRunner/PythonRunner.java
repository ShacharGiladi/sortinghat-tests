package org.bsmh.sortinghat.backend.Runner.LanguagesRunner;

import org.bsmh.sortinghat.backend.Runner.Exceptions.EvilCodeException;
import org.springframework.beans.factory.annotation.Value;
import java.io.IOException;
import java.util.List;
import java.util.Arrays;


public class PythonRunner extends CodeRunner {

    @Value("${runner.python}")
    private String pythonCommand;

    @Override
    public List<String> run(String fileLocation, List<String> processInputs, String userId) throws Exception {
        super.setCodeRunner(this.pythonCommand);
        return super.run(fileLocation, processInputs, userId);
    }

    @Override
    public void checkIfCodeIsEvil(String code) throws  EvilCodeException {
        String[] keywords = new String[] {"base64", "Cmd", "os.", "sys.", "socket", "win32",
                "subprocess", "fs", "scapy", "eval", "exec"};

        super.checkIfCodeIsEvil(code, Arrays.asList(keywords));
    }
}
