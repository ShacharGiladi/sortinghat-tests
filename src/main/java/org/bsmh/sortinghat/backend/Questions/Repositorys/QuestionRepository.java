package org.bsmh.sortinghat.backend.Questions.Repositorys;

import org.bsmh.sortinghat.backend.Questions.Models.Question;
import org.springframework.data.jpa.repository.JpaRepository;

public interface QuestionRepository extends JpaRepository<Question, Long> {}
