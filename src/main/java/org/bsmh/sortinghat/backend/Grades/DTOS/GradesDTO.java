package org.bsmh.sortinghat.backend.Grades.DTOS;

public class GradesDTO {
    private String username;
    private String id;
    private double grade;

    public GradesDTO(String id, String username, double grade) {
        this.username = username;
        this.id = id;
        this.grade = grade;
    }

    public String getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }



    public void setId(String id) {
        this.id = id;
    }

    public double getGrade() {
        return grade;
    }

    public void setGrade(double grade) {
        this.grade = grade;
    }
}
