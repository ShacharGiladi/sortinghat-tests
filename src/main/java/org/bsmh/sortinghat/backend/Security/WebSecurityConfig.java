package org.bsmh.sortinghat.backend.Security;

import org.bsmh.sortinghat.backend.Users.Models.User;
import org.bsmh.sortinghat.backend.Users.Services.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.access.AccessDeniedHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    private JwtTokenProvider jwtTokenProvider;

    @Autowired
    UserService userService;

    @Value("${roles.student}")
    String STUDENT_ROLE;

    @Value("${roles.guardian}")
    String GUARDIAN_ROLE;

    @Value("${roles.sortingAdmin}")
    String SORTING_ADMIN_ROLE;

    @Value("${roles.admin}")
    String ADMIN_ROLE;

    Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        // Disable CSRF (cross site request forgery)
        http.csrf().disable();

        // No session will be created or used by spring security
        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);

        http.authorizeRequests()
                .antMatchers("/api/users/login").permitAll()//
                .antMatchers("/api/grades/**").hasAnyAuthority(GUARDIAN_ROLE, SORTING_ADMIN_ROLE, ADMIN_ROLE)
                .antMatchers("/api/runner/**").hasAnyAuthority(STUDENT_ROLE)
                .antMatchers("/api/cycles/**").hasAnyAuthority(SORTING_ADMIN_ROLE, ADMIN_ROLE,GUARDIAN_ROLE)
                .antMatchers(HttpMethod.GET, "/api/cases/**").hasAnyAuthority(GUARDIAN_ROLE, SORTING_ADMIN_ROLE, ADMIN_ROLE)
                .antMatchers(HttpMethod.POST, "/api/cases/**").hasAnyAuthority(SORTING_ADMIN_ROLE, ADMIN_ROLE)
                .antMatchers(HttpMethod.POST, "/api/tests/**").hasAnyAuthority(SORTING_ADMIN_ROLE, ADMIN_ROLE)
                .antMatchers(HttpMethod.GET, "/api/tests/**").hasAnyAuthority(STUDENT_ROLE, SORTING_ADMIN_ROLE, ADMIN_ROLE, GUARDIAN_ROLE)
                .antMatchers(HttpMethod.POST, "/api/users/**").hasAnyAuthority(SORTING_ADMIN_ROLE, ADMIN_ROLE,GUARDIAN_ROLE)
                .antMatchers("/api/users").hasAnyAuthority( SORTING_ADMIN_ROLE, ADMIN_ROLE)
                // Disallow everything else..
                .anyRequest().authenticated();

        // If a user try to access a resource without having enough permissions
        http.exceptionHandling().accessDeniedHandler(new AccessDeniedHandler() {
            @Override
            public void handle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, AccessDeniedException e) throws IOException, ServletException {
                httpServletResponse.setStatus(403);
                User user = userService.getUserFromToken(httpServletRequest);
                logger.info("unauthorized user request with user id " + user.getId() + " requested " + httpServletRequest.getMethod() + " " + httpServletRequest.getRequestURI() + " has role: " + user.getRole());
            }
        });

        // Apply JWT
        http.apply(new JwtTokenFilterConfigurer(jwtTokenProvider));

        // Optional, if you want to test the API from a browser
        //http.httpBasic();
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        // Allow swagger to be accessed without authentication
        web.ignoring().antMatchers("/public/**", "/js/**", "/css/**", "/fonts/**", "/img/**", "/charts/**", "/favicon.ico", "/editor.worker.js", "/");
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder(12);
    }
}
