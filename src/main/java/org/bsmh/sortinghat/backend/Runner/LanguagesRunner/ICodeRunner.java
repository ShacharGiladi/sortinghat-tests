package org.bsmh.sortinghat.backend.Runner.LanguagesRunner;

import java.util.List;

public interface ICodeRunner {
    public List<String> run(String fileLocation, List<String> processInputs, String userId) throws Exception;
}
