package org.bsmh.sortinghat.backend.Users.Models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.bsmh.sortinghat.backend.Cycles.Cycle;
import org.bsmh.sortinghat.backend.Tests.Test;
import org.bsmh.sortinghat.backend.Users.DTOS.FullUserInfoDTO;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.util.*;
import java.util.stream.Collectors;

@Entity
@Table(name = "Users")
public class User {
    @Id
    @Column(name = "id")
    private String id;

    @Column(name = "username")
    private String username;

    @Column(name = "password")
    private String password;

    @Column(name = "finished_interview")
    private Boolean finishedInterview = false;

    @ManyToMany(mappedBy = "usersInCycle")
    @JsonIgnore
    private Set<Cycle> studentsCycles;

    @Column(name = "role_id")
    private int role;

    @OneToMany
    @JoinColumn(name = "user_id")
    private List<UsersTimes> usersTimes;

    @OneToMany
    @JoinColumn(name = "user_id")
    private List<UsersBreaks> usersBreaks;

    @OneToMany
    @JoinColumn(name = "user_id")
    private List<UsersInCycleAndTest> usersTests;

    public User(String id) {
        this.id = id;
    }

    public User() {
        this.studentsCycles = new HashSet<>();
    }

    public Set<Cycle> getStudentsCycles() {
        return studentsCycles;
    }

    public void setStudentsCycles(Set<Cycle> studentsCycles) {
        this.studentsCycles = studentsCycles;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @JsonIgnore
    public List<UsersTimes> getUsersTimes() {
        return usersTimes;
    }

    public List<UsersTimes> getTime() {

        return this.usersTimes;
    }


    public String getUsername() {
        return username;
    }

    @JsonIgnore
    public String getPassword() {
        return password;
    }

    @JsonProperty
    public void setPassword(String password) {
        this.password = password;
    }

    public int getRole() {
        return role;
    }

    public void setRole(int role) {
        this.role = role;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public List<UsersBreaks> getUsersBreaks() {
        return usersBreaks;
    }

    @JsonIgnore
    public boolean getIsUserInBreak() {
        for (UsersBreaks usersBreak : this.usersBreaks) {
            if (!usersBreak.getDidReturn()) {
                return true;
            }
        }

        return false;
    }

    @JsonIgnore
    public List<Test> getUsersTests() {
        return this.usersTests.stream().map(usersInCycleAndTest -> usersInCycleAndTest.getTest()).collect(Collectors.toList());
    }

    @JsonIgnore
    public List<UsersInCycleAndTest> getUsersInCycleInTest() {
        return this.usersTests;
    }

    @Override
    public String toString() {
        return this.id;
    }

    public Boolean getFinishedInterview() {
        return finishedInterview;
    }

    public void setFinishedInterview(Boolean finishedInterview) {
        this.finishedInterview = finishedInterview;
    }

    public void matchToFullUserInfoDTO(FullUserInfoDTO userInfo) {
        this.username = userInfo.username;
        this.password = userInfo.password;
        this.role = userInfo.role;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(id, user.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
