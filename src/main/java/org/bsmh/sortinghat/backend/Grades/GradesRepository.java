package org.bsmh.sortinghat.backend.Grades;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface GradesRepository extends JpaRepository<StudentsGrades, Long> {

    @Query(value = "SELECT * FROM answers_per_student_in_cycle WHERE cycle_id = ?1 AND test_id = ?2 ORDER BY student_id ASC, question_id ASC", nativeQuery = true)
    List<StudentsGrades> findByCycleTest(Long cycleId, Long testId);

    @Query(value = "SELECT * FROM answers_per_student_in_cycle WHERE cycle_id = ?1 AND test_id = ?2 AND question_id = ?3 AND case_id = ?4", nativeQuery = true)
    Optional<StudentsGrades> getByAnswer(Long cycleId, Long testId, Long questionId, Long caseId);

    @Query(value = "SELECT * FROM answers_per_student_in_cycle WHERE cycle_id = ?1 AND test_id = ?2 AND student_id = ?3", nativeQuery = true)
    List<StudentsGrades> getUserAnswers(Long cycleId, Long testId, String userId);

    @Query(value = "SELECT * FROM answers_per_student_in_cycle WHERE cycle_id = ?1 AND test_id = ?2 AND  case_id  = ?3 AND student_id = ?4", nativeQuery = true)
    Optional<StudentsGrades> getUserAnswer(Long cycleId, Long testId,Long caseId, String userId);

    @Query(value = "SELECT * FROM answers_per_student_in_cycle WHERE cycle_id = ?1 AND test_id = ?2 AND  question_id  = ?3 AND student_id = ?4", nativeQuery = true)
    List<StudentsGrades> getCases(Long cycleId, Long testId,Long questId, String userId);

    List<StudentsGrades> findByCycleIdAndTestIdOrderByUser(Long cycleId, Long testId);

}
