package org.bsmh.sortinghat.backend.Users.Services;

import javassist.NotFoundException;
import org.bsmh.sortinghat.backend.Users.Models.UsersCode;
import org.bsmh.sortinghat.backend.Users.Models.UsersInCycleAndTest;
import org.bsmh.sortinghat.backend.Users.Repositorys.UserCodeRepository;
import org.bsmh.sortinghat.backend.Users.Repositorys.UsersCycleAndTestRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UsersCodeService {
    @Autowired
    UserCodeRepository userCodeRepository;

    public void submitCode(Long cycleId, Long testId, String userId, String fileExertion, String code, long questionId) {
        Optional<UsersCode> usersCodeOptional = userCodeRepository.findByCycleIdAndTestIdAndStudentIdAndQuestionIdAndLanguageOfCode(cycleId, testId, userId, questionId, fileExertion);
        if(usersCodeOptional.isEmpty()){
            userCodeRepository.save(new UsersCode(cycleId,userId, code,testId, fileExertion, questionId));
        } else {
            UsersCode usersCode = usersCodeOptional.get();
            usersCode.setCode(code);
            userCodeRepository.save(usersCode);
        }
    }

    public UsersCode getUserCode(String userId, Long cycleId, Long testId, Long questionId) throws NotFoundException {
        Optional<UsersCode> usersCode = userCodeRepository.findByCycleIdAndTestIdAndStudentIdAndQuestionId(cycleId, testId, userId, questionId);;
        if(usersCode.isEmpty()) {
            throw new NotFoundException("users code was not found");
        }

        return usersCode.get();

    }

    public List<UsersCode> getUsersCodeByTestAndCycle(Long testId, Long cycleId, String studentId) {
        return userCodeRepository.findByCycleIdAndTestIdAndStudentId(cycleId, testId, studentId);
    }
}
