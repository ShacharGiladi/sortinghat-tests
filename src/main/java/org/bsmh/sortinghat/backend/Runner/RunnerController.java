package org.bsmh.sortinghat.backend.Runner;

import org.bsmh.sortinghat.backend.Loggers.RequestLogger;
import org.bsmh.sortinghat.backend.Runner.DTO.CheckResultDTO;
import org.bsmh.sortinghat.backend.Runner.DTO.CompilationResultDTO;
import org.bsmh.sortinghat.backend.Runner.DTO.UserResponseDTO;
import org.bsmh.sortinghat.backend.Runner.Exceptions.ErrorInProcessException;
import org.bsmh.sortinghat.backend.Runner.Exceptions.EvilCodeException;
import org.bsmh.sortinghat.backend.Runner.Exceptions.InfiniteLoopException;
import org.bsmh.sortinghat.backend.Runner.Exceptions.SyntaxErrorException;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.security.Principal;

@RestController
@RequestMapping("/api/runner")
public class RunnerController {

    @Autowired
    RunnerService runnerService;

    private RequestLogger logger;

    public RunnerController() {
        this.logger = new RequestLogger(LoggerFactory.getLogger(this.getClass()));
    }

    @PostMapping("/submit")
    public UserResponseDTO submit(@RequestBody RunnerRequestData data, Principal principal) throws Exception {
        logger.info(principal, "submit code in questionID: " + data.getQuestionId() +
                                    ", testID: " + data.getTestId() +
                                    ", fileExtention: " + data.getFileExtension());
        return runnerService.submit(data, principal);
    }

    @PostMapping("/check")
    public CheckResultDTO check(@RequestBody RunnerRequestData data, Principal principal) throws Exception {
        logger.info(principal, "check code in questionID: " + data.getQuestionId() +
                ", testID: " + data.getTestId() +
                ", fileExtention: " + data.getFileExtension());
        return runnerService.check(data,principal);
    }

    @PostMapping("/compile")
    public CompilationResultDTO compile(@RequestBody RunnerRequestData data, Principal principal) throws Exception {
        logger.info(principal, "compile code in questionID: " + data.getQuestionId() +
                ", testID: " + data.getTestId() +
                ", fileExtention: " + data.getFileExtension());
        return runnerService.compile(data, principal);
    }
}
