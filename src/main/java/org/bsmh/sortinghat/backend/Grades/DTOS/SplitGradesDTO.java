package org.bsmh.sortinghat.backend.Grades.DTOS;

public class SplitGradesDTO {

    private String question;
    private Long quest_case;
    private double totalWeight;
    private boolean isCorrect;

    public SplitGradesDTO(String question, Long quest_case, double totalWeight, boolean isCorrect) {
        this.question = question;
        this.quest_case = quest_case;
        this.totalWeight = totalWeight;
        this.isCorrect = isCorrect;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public Long getQuest_case() {
        return quest_case;
    }

    public void setQuest_case(Long quest_case) {
        this.quest_case = quest_case;
    }

    public double getTotalWeight() {
        return totalWeight;
    }

    public void setTotalWeight(double totalWeight) {
        this.totalWeight = totalWeight;
    }

    public boolean isCorrect() {
        return isCorrect;
    }

    public void setCorrect(boolean correct) {
        isCorrect = correct;
    }
}
