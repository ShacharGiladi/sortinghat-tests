package org.bsmh.sortinghat.backend.Cycles;

import javassist.NotFoundException;
import org.bsmh.sortinghat.backend.EntityController;
import org.bsmh.sortinghat.backend.Tests.Test;
import org.bsmh.sortinghat.backend.Users.Models.User;
import org.bsmh.sortinghat.backend.Tests.TestRepository;
import org.bsmh.sortinghat.backend.Users.Repositorys.UserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.security.Principal;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/api/cycles")
public class CycleController extends EntityController<Cycle, Long> {

    @Autowired private CycleService cycleService;


    @GetMapping("/active")
    public List<Cycle> getAllActiveCycles(Principal principal){
        return cycleService.getAllActiveCycles(principal);
    }

    @GetMapping("/{cycleId}/users")
    public Set<User> getAllUsersInCycle(@PathVariable Long cycleId, Principal principal) {
        this.logger.debug(principal,"Requested all of the Users in cycle for cycle= " + cycleId);
        return cycleService.getAllUsersInCycle(cycleId, principal);
    }

    @RequestMapping(value = "/{cycleId}/tests", method = RequestMethod.GET)
    public List<Test> getAllTestsInCycle(@PathVariable Long cycleId, Principal principal) {
        this.logger.debug(principal, "Requested all of the tests in cycle for cycle=" + cycleId);
        return cycleService.getTestsInCycle(cycleId, principal);
    }

    @PostMapping("/{cycleId}/tests")
    public void setTestsForCycle(@PathVariable Long cycleId, @RequestBody List<Test> tests, Principal principal) {
        this.logger.debug(principal, "Requested to set tests for the specific cycle= " + cycleId);
        cycleService.setTestForCycle(cycleId, tests, principal);
    }

    @PostMapping("/{cycleId}/users")
    public void setUsersForCycle(@PathVariable Long cycleId, @RequestBody Set<User> users, Principal principal) {
        this.logger.debug(principal,"Requested to set users to the specific cycle= "+ cycleId);
        cycleService.setUsersForCycle(cycleId, users, principal);
    }

    @RequestMapping(value = "/{cycleId}/test/{testId}", method = RequestMethod.PUT)
    public void addTestToCycle(@PathVariable Long cycleId, @PathVariable Long testId, Principal principal) {
        this.logger.debug(principal,"Requested to add test " + testId + " to cycle " + cycleId);
        cycleService.addTestToCycle(cycleId, testId, principal);
    }

    @PutMapping("/{cycleId}/user/{userId}")
    public void addUserToCycle(@PathVariable Long cycleId, @PathVariable String userId, Principal principal) {
        this.logger.debug(principal, "Requested to add user to the specific cycle= " + cycleId);
        cycleService.addUserToCycle(cycleId, userId, principal);
    }

    @Override
    public Cycle update(@RequestBody Cycle entity, Principal principal) {
        this.logger.info(principal, " update request, entity = " + entity.toString());
        try {
            Cycle cycle = this.cycleService.getById(entity.getCycleId());
            cycle.copyCycleData(entity);
            return this.getRepository().save(cycle);
        } catch (NotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "cycle not found");
        }
    }

    @Override
    protected JpaRepository<Cycle, Long> getRepository() {
        return cycleService.cycleRepository();
    }
}
