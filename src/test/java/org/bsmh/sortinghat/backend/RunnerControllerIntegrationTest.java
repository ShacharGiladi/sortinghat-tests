package org.bsmh.sortinghat.backend;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.bsmh.sortinghat.backend.Runner.DTO.CheckResultDTO;
import org.bsmh.sortinghat.backend.Runner.DTO.CompilationResultDTO;
import org.bsmh.sortinghat.backend.Runner.DTO.UserResponseDTO;
import org.bsmh.sortinghat.backend.Runner.Exceptions.EvilCodeException;
import org.bsmh.sortinghat.backend.Runner.RunnerController;
import org.bsmh.sortinghat.backend.Runner.RunnerRequestData;
import org.bsmh.sortinghat.backend.Runner.RunnerService;
import org.bsmh.sortinghat.backend.costumAssertions.RunnerRequestDataAssert;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.server.ResponseStatusException;


import java.security.Principal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@WebMvcTest(controllers = RunnerController.class, secure = false)
public class RunnerControllerIntegrationTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private RunnerService runnerService;

    @Test
    public void check_InvalidRequest() throws Exception  {
        Principal pricipal = new Principal() {
            @Override
            public String getName() {
                return "969786876";
            }
        };

        mvc.perform(post("/api/runner/check")
                .principal(pricipal))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void check_ValidInput() throws Exception  {

        RunnerRequestData data = new RunnerRequestData(
                1L,
                "def main(): pass",
                1L,
                "py",
                1L,
                Arrays.asList("1", "2")
        );
        Principal pricipal = new Principal() {
            @Override
            public String getName() {
                return "969786876";
            }
        };

        mvc.perform(post("/api/runner/check")
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(data))
                .principal(pricipal))
                .andExpect(status().isOk());
    }

    @Test
    public void check_invalidInput() throws Exception  {

        RunnerRequestData data = new RunnerRequestData(
                1L,
                null,
                1L,
                "py",
                1L,
                Arrays.asList("1", "2")
        );

        Principal pricipal = new Principal() {
            @Override
            public String getName() {
                return "969786876";
            }
        };

        mvc.perform(post("/api/runner/check")
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(data))
                .principal(pricipal))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void check_validateBLCall() throws Exception  {

        RunnerRequestData data = new RunnerRequestData(
                1L,
                "def main(): pass",
                1L,
                "py",
                1L,
                Arrays.asList("1", "2")
        );
        Principal pricipal = new Principal() {
            @Override
            public String getName() {
                return "969786876";
            }
        };

        mvc.perform(post("/api/runner/check")
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(data))
                .principal(pricipal));

        ArgumentCaptor<RunnerRequestData> dataCaptor = ArgumentCaptor.forClass(RunnerRequestData.class);
        verify(runnerService, times(1)).check(dataCaptor.capture(), eq(pricipal));
        RunnerRequestDataAssert.assertThat(dataCaptor.getValue()).allEquals(data);
    }

    @Test
    public void check_validInputReturnsResponseWithOutputs() throws Exception {
        RunnerRequestData data = new RunnerRequestData(
                1L,
                "def main(): pass",
                1L,
                "py",
                1L,
                Arrays.asList("1", "2")
        );

        Principal pricipal = new Principal() {
            @Override
            public String getName() {
                return "969786876";
            }
        };

        CheckResultDTO checkResultDTO = new CheckResultDTO(Arrays.asList("2"));

        //given(runnerService.check(data, any())).willReturn(checkResultDTO);
        doReturn(checkResultDTO).when(runnerService).check(any(), any());

        MvcResult mvcResult = mvc.perform(post("/api/runner/check")
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(data))
                .principal(pricipal))
                .andReturn();

        String expected = objectMapper.writeValueAsString(checkResultDTO);
        String actual = mvcResult.getResponse().getContentAsString();

        Assert.assertEquals(expected, actual);

    }

    @Test
    public void check_evilCode() throws Exception {
        RunnerRequestData data = new RunnerRequestData(
                1L,
                "def main(): pass",
                1L,
                "py",
                1L,
                Arrays.asList("1", "2")
        );

        Principal pricipal = new Principal() {
            @Override
            public String getName() {
                return "969786876";
            }
        };

        doThrow(new ResponseStatusException(HttpStatus.FORBIDDEN, "")).when(runnerService).check(any(), any());

        mvc.perform(post("/api/runner/check")
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(data))
                .principal(pricipal))
                .andExpect(status().isForbidden());

    }


    // -----------------------------------------------------


    @Test
    public void submit_InvalidRequest() throws Exception  {
        Principal pricipal = new Principal() {
            @Override
            public String getName() {
                return "969786876";
            }
        };

        mvc.perform(post("/api/runner/submit")
                .principal(pricipal))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void submit_ValidInput() throws Exception  {

        RunnerRequestData data = new RunnerRequestData(
                1L,
                "def main(): pass",
                1L,
                "py",
                1L,
                Arrays.asList("1", "2")
        );
        Principal pricipal = new Principal() {
            @Override
            public String getName() {
                return "969786876";
            }
        };

        mvc.perform(post("/api/runner/submit")
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(data))
                .principal(pricipal))
                .andExpect(status().isOk());
    }

    @Test
    public void submit_invalidInput() throws Exception  {

        RunnerRequestData data = new RunnerRequestData(
                1L,
                null,
                1L,
                "py",
                1L,
                Arrays.asList("1", "2")
        );

        Principal pricipal = new Principal() {
            @Override
            public String getName() {
                return "969786876";
            }
        };

        mvc.perform(post("/api/runner/submit")
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(data))
                .principal(pricipal))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void submit_validateBLCall() throws Exception  {

        RunnerRequestData data = new RunnerRequestData(
                1L,
                "def main(): pass",
                1L,
                "py",
                1L,
                Arrays.asList("1", "2")
        );
        Principal pricipal = new Principal() {
            @Override
            public String getName() {
                return "969786876";
            }
        };

        mvc.perform(post("/api/runner/submit")
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(data))
                .principal(pricipal));

        ArgumentCaptor<RunnerRequestData> dataCaptor = ArgumentCaptor.forClass(RunnerRequestData.class);
        verify(runnerService, times(1)).submit(dataCaptor.capture(), eq(pricipal));
        RunnerRequestDataAssert.assertThat(dataCaptor.getValue()).allEquals(data);
    }

    @Test
    public void submit_validInputReturnsResponseSuccess() throws Exception {
        RunnerRequestData data = new RunnerRequestData(
                1L,
                "def main(): pass",
                1L,
                "py",
                1L,
                Arrays.asList("1", "2")
        );

        Principal pricipal = new Principal() {
            @Override
            public String getName() {
                return "969786876";
            }
        };

        UserResponseDTO userResponseDTO = new UserResponseDTO(5, 5, new HashMap<>(), new ArrayList<>());

        doReturn(userResponseDTO).when(runnerService).submit(any(), any());

        MvcResult mvcResult = mvc.perform(post("/api/runner/submit")
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(data))
                .principal(pricipal))
                .andReturn();

        String expected = objectMapper.writeValueAsString(userResponseDTO);
        String actual = mvcResult.getResponse().getContentAsString();

        Assert.assertEquals(expected, actual);

    }

    @Test
    public void submit_evilCode() throws Exception {
        RunnerRequestData data = new RunnerRequestData(
                1L,
                "def main(): pass",
                1L,
                "py",
                1L,
                Arrays.asList("1", "2")
        );

        Principal pricipal = new Principal() {
            @Override
            public String getName() {
                return "969786876";
            }
        };

        doThrow(new ResponseStatusException(HttpStatus.FORBIDDEN, "")).when(runnerService).submit(any(), any());

        mvc.perform(post("/api/runner/submit")
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(data))
                .principal(pricipal))
                .andExpect(status().isForbidden());

    }

    // ------------------------------------


    @Test
    public void comile_InvalidRequest() throws Exception  {
        Principal pricipal = new Principal() {
            @Override
            public String getName() {
                return "969786876";
            }
        };

        mvc.perform(post("/api/runner/compile")
                .principal(pricipal))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void compile_ValidInput() throws Exception  {

        RunnerRequestData data = new RunnerRequestData(
                1L,
                "def main(): pass",
                1L,
                "py",
                1L,
                Arrays.asList("1", "2")
        );
        Principal pricipal = new Principal() {
            @Override
            public String getName() {
                return "969786876";
            }
        };

        mvc.perform(post("/api/runner/compile")
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(data))
                .principal(pricipal))
                .andExpect(status().isOk());
    }

    @Test
    public void compile_invalidInput() throws Exception  {

        RunnerRequestData data = new RunnerRequestData(
                1L,
                null,
                1L,
                "py",
                1L,
                Arrays.asList("1", "2")
        );

        Principal pricipal = new Principal() {
            @Override
            public String getName() {
                return "969786876";
            }
        };

        mvc.perform(post("/api/runner/compile")
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(data))
                .principal(pricipal))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void compile_validateBLCall() throws Exception  {

        RunnerRequestData data = new RunnerRequestData(
                1L,
                "def main(): pass",
                1L,
                "py",
                1L,
                Arrays.asList("1", "2")
        );
        Principal pricipal = new Principal() {
            @Override
            public String getName() {
                return "969786876";
            }
        };

        mvc.perform(post("/api/runner/compile")
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(data))
                .principal(pricipal));

        ArgumentCaptor<RunnerRequestData> dataCaptor = ArgumentCaptor.forClass(RunnerRequestData.class);
        verify(runnerService, times(1)).compile(dataCaptor.capture(), eq(pricipal));
        RunnerRequestDataAssert.assertThat(dataCaptor.getValue()).allEquals(data);
    }

    @Test
    public void compile_validInputReturnsResponseSuccess() throws Exception {
        RunnerRequestData data = new RunnerRequestData(
                1L,
                "def main(): pass",
                1L,
                "py",
                1L,
                Arrays.asList("1", "2")
        );

        Principal pricipal = new Principal() {
            @Override
            public String getName() {
                return "969786876";
            }
        };

        CompilationResultDTO compilationResultDTO = new CompilationResultDTO();

        doReturn(compilationResultDTO).when(runnerService).compile(any(), any());

        MvcResult mvcResult = mvc.perform(post("/api/runner/compile")
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(data))
                .principal(pricipal))
                .andReturn();

        String expected = objectMapper.writeValueAsString(compilationResultDTO);
        String actual = mvcResult.getResponse().getContentAsString();

        Assert.assertEquals(expected, actual);

    }
}
