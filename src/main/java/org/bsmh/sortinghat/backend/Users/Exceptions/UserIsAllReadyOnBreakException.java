package org.bsmh.sortinghat.backend.Users.Exceptions;

public class UserIsAllReadyOnBreakException extends Exception {
    public UserIsAllReadyOnBreakException(String userId) {
        super("user " + userId + " is already on break");
    }
}
