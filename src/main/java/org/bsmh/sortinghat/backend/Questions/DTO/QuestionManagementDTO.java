package org.bsmh.sortinghat.backend.Questions.DTO;

import org.bsmh.sortinghat.backend.Questions.Models.Case;
import org.bsmh.sortinghat.backend.Questions.Models.Question;
import org.bsmh.sortinghat.backend.Questions.Models.QuestionInTest;

import java.util.List;

public class QuestionManagementDTO {
    public String description;
    public boolean isDemo;
    public Long id;
    public List<Case> cases;
    public String name;

    public QuestionManagementDTO(Question question) {
        this.description = question.getDescription();
        this.isDemo = question.isDemo();
        this.cases = question.getCases();
        this.id = question.getId();
        this.name = question.getName();
    }
}
