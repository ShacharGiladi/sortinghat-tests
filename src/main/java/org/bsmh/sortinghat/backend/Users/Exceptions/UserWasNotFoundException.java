package org.bsmh.sortinghat.backend.Users.Exceptions;

public class UserWasNotFoundException extends Exception {

    public UserWasNotFoundException(String userId) {
        super("user with " + userId + " was not found");
    }
}
