package org.bsmh.sortinghat.backend.Users.DTOS;

import org.bsmh.sortinghat.backend.Grades.StudentsGrades;

public class StudentCasesDTO {

    public Long questionCase;

    public Long questionId;

    public Long testId;

    public boolean isCorrect;

    public StudentCasesDTO(StudentsGrades studentsGrades) {
        this.questionCase = studentsGrades.getCase().getCaseId();
        this.questionId = studentsGrades.getQuestion().getId();
        this.testId = studentsGrades.getTestId();
        this.isCorrect = studentsGrades.getIsCorrect();
    }
}
