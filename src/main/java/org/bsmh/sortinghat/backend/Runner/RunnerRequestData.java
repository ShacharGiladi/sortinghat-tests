package org.bsmh.sortinghat.backend.Runner;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.Principal;
import java.util.List;
import java.util.Map;

public class RunnerRequestData {
    private Long questionId;
    private String code;
    private Long testId;
    private String fileExtension;
    private Long cycleId;
    private List<String> inputs;

    public RunnerRequestData(Long questionId, String code, Long testId, String fileExtension, Long cycleId, List<String> inputs) {
        this.questionId = questionId;
        this.code = code;
        this.testId = testId;
        this.fileExtension = fileExtension;
        this.cycleId = cycleId;
        this.inputs = inputs;
    }

    public Long getQuestionId() {
        return questionId;
    }

    public void setQuestionId(Long questionId) {
        this.questionId = questionId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Long getTestId() {
        return testId;
    }

    public void setTestId(Long testId) {
        this.testId = testId;
    }

    public String getFileExtension() {
        return fileExtension;
    }

    public void setFileExtension(String fileExtension) {
        this.fileExtension = fileExtension;
    }

    public Long getCycleId() {
        return cycleId;
    }

    public void setCycleId(Long cycleId) {
        this.cycleId = cycleId;
    }

    public List<String> getInputs() {
        return inputs;
    }

    public void setInputs(List<String> inputs) {
        this.inputs = inputs;
    }
}
