package org.bsmh.sortinghat.backend.Cycles;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.bsmh.sortinghat.backend.Tests.Test;
import org.bsmh.sortinghat.backend.Users.Models.User;

@Entity
@Table(name = "Cycle")
public class Cycle {
    @Id
    @Column(name = "cycle_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long cycleId;

    @Column(name = "cycle_name")
    private String name;

    @Column(name = "start_date")
    private Date startDate;

    @Column(name = "end_date")
    private Date endDate;


    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "tests_in_cycle",
            joinColumns = {@JoinColumn(name = "cycle_id")},
            inverseJoinColumns = {@JoinColumn(name = "test_id")})
    private List<Test> testsInCycle;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "student_in_cycle",
            joinColumns = {@JoinColumn(name = "cycle_id")},
            inverseJoinColumns = {@JoinColumn(name = "user_id")})
    @JsonIgnore
    private Set<User> usersInCycle;

    public Cycle() {}

    public Cycle(Long cycleId) {
        this.cycleId = cycleId;
    }

    public Cycle(String name, Date startDate, Date endDate) {
        this.name = name;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public void copyCycleData(Cycle cycle) {
        this.name = cycle.getName();
        this.startDate = cycle.getStartDate();
        this.endDate = cycle.getEndDate();
        this.testsInCycle = cycle.getTestsInCycle();
    }



    public Long getCycleId() {
        return cycleId;
    }

    public void setCycleId(Long cycleId) {
        this.cycleId = cycleId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Set<User> getUsersInCycle() {
        return usersInCycle;
    }

    public void setUsersInCycle(Set<User> usersInCycle) {
        this.usersInCycle = usersInCycle;
    }

    public List<Test> getTestsInCycle() {
        return testsInCycle;
    }

    public void setTestsInCycle(List<Test> testsInCycle) {
        this.testsInCycle = testsInCycle;
    }

    public void addTestInCycle(Test testToAdd) {
        this.testsInCycle.add(testToAdd);
    }

    public void addUserInCycle(User userToAdd) {
        this.usersInCycle.add(userToAdd);
    }
}
