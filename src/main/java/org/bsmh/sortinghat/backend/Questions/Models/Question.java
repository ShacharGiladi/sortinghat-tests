package org.bsmh.sortinghat.backend.Questions.Models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "Questions")
public class Question {
    @Id
    @Column(name = "question_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "question_name")
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "is_demo")
    private boolean isDemo;

    @OneToMany
    @JoinColumn(name = "question_id")
    @JsonIgnore
    private List<Case> cases;

    @OneToMany(mappedBy = "question")
    private List<QuestionInTest> questionInTests;

    public void setQuestionInTests(List<QuestionInTest> questionInTests) {
        this.questionInTests = questionInTests;
    }

    public boolean isDemo() {
        return isDemo;
    }

    public void setDemo(boolean demo) {
        isDemo = demo;
    }

    @JsonIgnore
    public double getWeight(Long testId) {
        for (QuestionInTest questionInTest : this.questionInTests) {
            if(questionInTest.getTest().getId() == testId) {
                return questionInTest.getWeightOfQuestion();
            }
        }
        return 0;
    }

    public void addCase(Case caseToAdd) {
        this.cases.add(caseToAdd);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Case> getCases() {
        return cases;
    }

    public void setCases(List<Case> cases) {
        this.cases = cases;
    }


    @Override
    public String toString() {
        return "Question{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
