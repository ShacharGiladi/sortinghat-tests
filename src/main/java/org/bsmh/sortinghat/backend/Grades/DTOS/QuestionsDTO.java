package org.bsmh.sortinghat.backend.Grades.DTOS;

import java.util.Date;

public class QuestionsDTO {
    private Long questionId;
    private int correctCases;
    private int totalCases;
    private String languageCode;
    private Date dateOfSubmit;
    private String questionName;

    public QuestionsDTO(Long questionId, int correctCases, int totalCases, String languageCode, Date dateOfSubmit, String questionName) {
        this.questionId = questionId;
        this.correctCases = correctCases;
        this.totalCases = totalCases;
        this.languageCode = languageCode;
        this.dateOfSubmit = dateOfSubmit;
        this.questionName = questionName;
    }

    public QuestionsDTO(GradesStatsDTO gradesStats, String questionName) {
        this.questionId = gradesStats.getQuestionId();
        this.correctCases = gradesStats.getCorrectCases();
        this.totalCases = gradesStats.getTotalCases();
        this.languageCode = gradesStats.getLanguageCode();
        this.dateOfSubmit = gradesStats.getDateOfSubmit();
        this.questionName = questionName;
    }

    public int getCorrectCases() {
        return correctCases;
    }

    public void setCorrectCases(int correctCases) {
        this.correctCases = correctCases;
    }

    public int getTotalCases() {
        return totalCases;
    }

    public void setTotalCases(int totalCases) {
        this.totalCases = totalCases;
    }

    public String getLanguageCode() {
        return languageCode;
    }

    public void setLanguageCode(String languageCode) {
        this.languageCode = languageCode;
    }

    public Date getDateOfSubmit() {
        return dateOfSubmit;
    }

    public void setDateOfSubmit(Date dateOfSubmit) {
        this.dateOfSubmit = dateOfSubmit;
    }

    public String getQuestionName() {
        return questionName;
    }

    public void setQuestionName(String questionName) {
        this.questionName = questionName;
    }

    public Long getQuestionId() {
        return questionId;
    }

    public void setQuestionId(Long questionId) {
        this.questionId = questionId;
    }
}
