package org.bsmh.sortinghat.backend.Users.DTOS;

import org.bsmh.sortinghat.backend.Users.Models.UsersInCycleAndTest;

public class UsersInCycleInTestDTO {
    public Long testId;
    public Long cycleId;
    public String userId;

    public UsersInCycleInTestDTO(UsersInCycleAndTest usersInCycleAndTest) {
        this.testId = usersInCycleAndTest.getTest().getId();
        this.cycleId = usersInCycleAndTest.getCycle().getCycleId();
        this.userId = usersInCycleAndTest.getUser().getId();
    }
}
