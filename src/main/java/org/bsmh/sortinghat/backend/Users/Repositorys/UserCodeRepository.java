package org.bsmh.sortinghat.backend.Users.Repositorys;

import org.bsmh.sortinghat.backend.Users.Models.UsersCode;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface UserCodeRepository extends JpaRepository<UsersCode, Long> {
    List<UsersCode> findByCycleIdAndTestIdAndStudentId(Long cycleId, Long testId, String studentId);
    Optional<UsersCode> findByCycleIdAndTestIdAndStudentIdAndQuestionIdAndLanguageOfCode(Long cycleId, Long testId, String studentId, Long questionId, String languageOfCode);
    Optional<UsersCode> findByCycleIdAndTestIdAndStudentIdAndQuestionId(Long cycleId, Long testId, String studentId, Long questionId);
    List<UsersCode> findByCycleIdAndTestId(Long cycleId, Long testId);
}

