package org.bsmh.sortinghat.backend.Runner.Exceptions;

import java.util.List;

public class ErrorInProcessException extends Exception {
    private List<String> errors;
    private List<String> processOutputList;

    public List<String> getProcessOutputList() {
        return processOutputList;
    }

    public List<String> getErrors() {
        return errors;
    }

    public ErrorInProcessException(List<String> errors, List<String> processOutputList) {
        super();
        this.errors = errors;
        this.processOutputList = processOutputList;
    }
}
