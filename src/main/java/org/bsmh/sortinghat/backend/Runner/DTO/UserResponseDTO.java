package org.bsmh.sortinghat.backend.Runner.DTO;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class UserResponseDTO {
    private int numOfSuccess;
    private int numOfCases;
    private HashMap<String, List<String>> errors;
    private List<String> syntaxErrors;


    public UserResponseDTO() {
        this.errors = new HashMap<>();
        this.syntaxErrors = new ArrayList<>();
    }

    public UserResponseDTO(int numOfSuccess, int numOfCases, HashMap<String, List<String>> errors, List<String> syntaxErrors) {
        this.numOfSuccess = numOfSuccess;
        this.numOfCases = numOfCases;
        this.errors = errors;
        this.syntaxErrors = syntaxErrors;
    }

    public int getNumOfSuccess() {
        return numOfSuccess;
    }

    public void setNumOfSuccess(int numOfSuccess) {
        this.numOfSuccess = numOfSuccess;
    }

    public int getNumOfCases() {
        return numOfCases;
    }

    public void setNumOfCases(int numOfCases) {
        this.numOfCases = numOfCases;
    }

    public HashMap<String, List<String>> getErrors() {
        return errors;
    }

    public void setErrors(HashMap<String, List<String>> errors) {
        this.errors = errors;
    }

    public List<String> getSyntaxErrors() {
        return syntaxErrors;
    }

    public void setSyntaxErrors(List<String> syntaxErrors) {
        this.syntaxErrors = syntaxErrors;
    }
}
