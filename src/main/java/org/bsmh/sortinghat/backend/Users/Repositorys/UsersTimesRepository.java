package org.bsmh.sortinghat.backend.Users.Repositorys;

import org.bsmh.sortinghat.backend.Users.Models.User;
import org.bsmh.sortinghat.backend.Users.Models.UsersTimes;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface UsersTimesRepository  extends JpaRepository<UsersTimes, Long> {
     Optional<UsersTimes> findByUserIdAndCycleIdAndTestId(String userId, Long cycleId, Long testId);
}
