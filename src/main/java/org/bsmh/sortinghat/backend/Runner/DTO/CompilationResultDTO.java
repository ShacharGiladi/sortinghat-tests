package org.bsmh.sortinghat.backend.Runner.DTO;

import org.bsmh.sortinghat.backend.Runner.Exceptions.SyntaxErrorException;

import java.util.List;

public class CompilationResultDTO
{
    private boolean compiledSuccessfully;
    private List<String> errors;

    public CompilationResultDTO() {
        this.compiledSuccessfully = true;
        this.errors = null;
    }

    public CompilationResultDTO(SyntaxErrorException ex) {
        this.compiledSuccessfully = false;
        this.errors = ex.getErrorList();
    }

    public boolean isCompiledSuccessfully() {
        return compiledSuccessfully;
    }

    public void setCompiledSuccessfully(boolean compiledSuccessfully) {
        this.compiledSuccessfully = compiledSuccessfully;
    }

    public List<String> getErrors() {
        return errors;
    }

    public void setErrors(List<String> errors) {
        this.errors = errors;
    }
}
