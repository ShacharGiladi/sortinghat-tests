package org.bsmh.sortinghat.backend.Runner.LanguagesRunner;

import org.bsmh.sortinghat.backend.Runner.Exceptions.ErrorInProcessException;
import org.bsmh.sortinghat.backend.Runner.Exceptions.SyntaxErrorException;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

public abstract class CodeWithCompileRunner extends CodeRunner {
    private String compilerCommand;

    @Override
    public List<String> run(String exeFileLocation, List<String> processInputs, String userId) throws Exception {
        super.setCodeRunner(exeFileLocation);
        this.isCompiling = false;
        return super.run(" ", processInputs, userId);
    }

    public String compileCode(String fileLocation, String userId) throws Exception {
        super.setCodeRunner(compilerCommand);
        this.isCompiling = true;
        List<String> completionOutputs = new ArrayList<>();

        try{
            completionOutputs = super.run(fileLocation, new ArrayList<>(), userId);
        } catch (ErrorInProcessException e) {
            if (e.getErrors().size() != 3 && e.getErrors().get(0) != "") {
                throw e;
            } else {
                completionOutputs = e.getProcessOutputList();
            }
        }

        int compilationErrorsLine = getCompilationErrorsLine(completionOutputs);

        if(compilationErrorsLine != -1) {
            throw new SyntaxErrorException(completionOutputs.subList(compilationErrorsLine, completionOutputs.size()));
        }

        String exeFileLocation = changeFileExtension(fileLocation, "exe");
        //this.moveExeFile(fileLocation, exeFileLocation);
        return exeFileLocation;
    }

    private void moveExeFile(String codeFileLocation, String exeFileLocation) throws IOException {
        File codeFile = new File(codeFileLocation);
        String parentDir = codeFile.getParent();
        File exeFile = new File(exeFileLocation);
        Files.move(Paths.get(new File("").getAbsolutePath(), exeFile.getName()), Paths.get(exeFile.getAbsolutePath()), REPLACE_EXISTING);
    }

    private int getCompilationErrorsLine(List<String> completionOutputs) {
        int lineOfError = 0;

        for (String currString: completionOutputs) {
            if (currString.toLowerCase().indexOf("error") != -1) {
                return lineOfError;
            }

            lineOfError++;
        }

        return -1;
    }

    public String getCompilerCommand() {
        return compilerCommand;
    }

    protected String changeFileExtension(String fileLocation, String fileExtension) {
        String[] splitLocation = fileLocation.split("\\.");
        splitLocation[1] = fileExtension;
        String exeFileLocation = String.join(".", splitLocation);
        return exeFileLocation;
    }
    

    public void setCompilerCommand(String compilerCommand) {
        this.compilerCommand = compilerCommand;
    }
}
