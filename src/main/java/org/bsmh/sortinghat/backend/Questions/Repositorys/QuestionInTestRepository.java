package org.bsmh.sortinghat.backend.Questions.Repositorys;

import org.bsmh.sortinghat.backend.Questions.Models.QuestionInTest;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface QuestionInTestRepository extends JpaRepository<QuestionInTest, Long> {
    List<QuestionInTest> findByTest_id(Long id);
}
