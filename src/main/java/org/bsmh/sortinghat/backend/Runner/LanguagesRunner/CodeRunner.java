package org.bsmh.sortinghat.backend.Runner.LanguagesRunner;


import org.apache.tomcat.util.buf.StringUtils;
import org.bsmh.sortinghat.backend.Runner.Exceptions.ErrorInProcessException;
import org.bsmh.sortinghat.backend.Runner.Exceptions.EvilCodeException;
import org.bsmh.sortinghat.backend.Runner.Exceptions.InfiniteLoopException;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;

public abstract class CodeRunner implements ICodeRunner {
    private String codeRunner;

    @Value("${runner.maxtime.case}")
    private int maxTimeOfProcess;
    private Logger logger = LoggerFactory.getLogger(this.getClass());
    protected boolean isCompiling = false;

    protected void setCodeRunner(String codeRunner) {
        this.codeRunner = codeRunner;
    }

    protected Boolean infiniteLoop = false;

    public CodeRunner() {
        logger.debug("new instance of runner");
    }

    @Override
    public List<String> run(String fileLocation, List<String> processInputs, String userId) throws Exception {
        ProcessBuilder processBuilder = new ProcessBuilder(this.codeRunner, fileLocation);
        processBuilder.directory(new File(fileLocation).getParentFile());

        Process process = processBuilder.start();

        logger.info(userId + " - starting process shell command : " + this.codeRunner + " " + fileLocation, " process id : ", process.pid());

        try (BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
             BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(process.getOutputStream()));
             BufferedReader errReader = new BufferedReader(new InputStreamReader(process.getErrorStream()))) {

            if (process.isAlive()) {
                logger.debug(userId + " writing to process id : " + process.pid() + " inputs : " + processInputs);
                this.writeToProcess(writer, processInputs);
            }

            logger.debug(userId + " - stating terminated thread for infinite loop process id: " + process.pid());

            if(!isCompiling) {
                Thread terminateThread = this.startTerminateThread(process);
            }


            List<String> processOutputs = this.getOutputFromProcess(reader);


            List<String> errors = this.getOutputFromProcess(errReader);

            if (this.infiniteLoop) {
                this.infiniteLoop = false;
                logger.info(userId + " - infinite loop after : " + this.getMaxTimeOfProcess() + " from process id : " + process.pid());
                throw new InfiniteLoopException();
            }

            logger.debug(userId + " - outputs from process id :" + process.pid() + " outputs : " + processOutputs);

            if (errors.size() != 0) {
                throw new ErrorInProcessException(errors, processOutputs);
            }

            return processOutputs;
        }
    }

    private List<String> getOutputFromProcess(BufferedReader reader) throws IOException {
        ArrayList<String> processOutputs = new ArrayList<>();
        String output;
        while (!this.infiniteLoop && (output = reader.readLine()) != null) {
            processOutputs.add(output);
        }

        return processOutputs;
    }

    private Thread startTerminateThread(Process process) {
        Thread terminateThread = new Thread(() -> {
            try {
                Thread.sleep(maxTimeOfProcess);
                if (process.isAlive()) {
                    infiniteLoop = true;
                    process.destroy();
                }
            } catch (InterruptedException e) {
            }
        });

        terminateThread.start();
        return terminateThread;
    }

    private void writeToProcess(BufferedWriter writer, List<String> listToWrite) throws IOException {
        for (String outputString : listToWrite) {
            writer.write(outputString);
            writer.write("\n");
            writer.flush();
        }
    }

    public int getMaxTimeOfProcess() {
        return maxTimeOfProcess;
    }

    public void setMaxTimeOfProcess(int maxTimeOfProcess) {
        this.maxTimeOfProcess = maxTimeOfProcess;
    }

    public abstract void checkIfCodeIsEvil(String code) throws EvilCodeException;

    protected void checkIfCodeIsEvil(String code, List<String> keywords) throws  EvilCodeException {
        String ip = "(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])";
        Pattern pattern = Pattern.compile("(" + StringUtils.join(keywords, '|') + "|" + ip + ")",
                Pattern.MULTILINE);

        ArrayList<String> errors = new ArrayList<>();
        String[] lineArray = code.split("\\r?\\n");

        int lineCounter = 1;

        for (String line : lineArray) {


            Matcher matcher = pattern.matcher(line);
            while (matcher.find()) {
                errors.add("Invalid word: " + matcher.group(0) + " in line " + lineCounter);
            }

            // read next line
            lineCounter++;
        }


        if (errors.size() > 0) {
            throw new EvilCodeException(errors);
        }
    }
}
