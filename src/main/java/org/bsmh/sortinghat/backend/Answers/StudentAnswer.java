package org.bsmh.sortinghat.backend.Answers;

import org.bsmh.sortinghat.backend.Cycles.Cycle;
import org.bsmh.sortinghat.backend.Tests.Test;
import org.bsmh.sortinghat.backend.Questions.Models.Case;
import org.bsmh.sortinghat.backend.Questions.Models.Question;
import org.bsmh.sortinghat.backend.Users.Models.User;

import javax.persistence.*;

@Entity
@Table(name = "answers_per_student_In_cycle")
public class StudentAnswer {
    @Id
    @Column(name = "answer_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long answerId;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @ManyToOne
    @JoinColumn(name = "cycle_id")
    private Cycle cycle;

    @ManyToOne
    @JoinColumn(name = "case_id")
    private Case answerCase;

    @ManyToOne
    @JoinColumn(name = "question_id")
    private Question answerQuestion;

    @ManyToOne
    @JoinColumn(name = "test_id")
    private Test answerTest;

    @Column(name = "weight_of_question")
    private double weight;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Cycle getCycle() {
        return cycle;
    }

    public void setCycle(Cycle cycle) {
        this.cycle = cycle;
    }

    public Case getAnswerCase() {
        return answerCase;
    }

    public void setAnswerCase(Case answerCase) {
        this.answerCase = answerCase;
    }

    @Override
    public String toString() {
        return "StudentAnswer{" +
                "answerId=" + answerId +
                ", user=" + user +
                ", cycle=" + cycle +
                ", answerCase=" + answerCase +
                ", answerQuestion=" + answerQuestion +
                ", answerTest=" + answerTest +
                ", weight=" + weight +
                '}';
    }
}
