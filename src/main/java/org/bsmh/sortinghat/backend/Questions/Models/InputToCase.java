package org.bsmh.sortinghat.backend.Questions.Models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Entity
@Table(name = "input_in_case")
public class InputToCase {

    @Id
    @Column(name = "input_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long inputId;

    @Column(name = "input")
    private String input;

    @Column(name = "case_id")
    private Long caseId;

    @Column(name = "order_of_input")
    private Long orderOfInput;

    public InputToCase() {}

    public String getInput() {
        return input;
    }

    public void setInput(String input) {
        this.input = input;
    }

    public Long getInputId() {
        return inputId;
    }

    public void setInputId(Long inputId) {
        this.inputId = inputId;
    }

    public Long getCaseId() {
        return caseId;
    }

    public void setCaseId(Long caseId) {
        this.caseId = caseId;
    }

    public Long getOrderOfInput() {
        return orderOfInput;
    }

    public void setOrderOfInput(Long orderOfInput) {
        this.orderOfInput = orderOfInput;
    }
    @Override
    public String toString() {
        return "InputToCase{" +
                "inputId=" + inputId +
                ", input='" + input + '\'' +
                '}';
    }
}
