package org.bsmh.sortinghat.backend.Loggers;

import org.slf4j.Logger;

import java.security.Principal;


public class RequestLogger {
    Logger logger;

    public RequestLogger(Logger logger) {
        this.logger = logger;
    }

    public void info(Principal principal, String log ) {
        logger.info("user: " + principal.getName() + ": "  + log);
    }

    public void debug(Principal principal, String log ) {
        logger.debug("user: " + principal.getName() + ": "  + log);
    }

    public void error(Principal principal, String log ) {
        logger.error("user: " + principal.getName() + ": "  + log);
    }

    public void warn(Principal principal, String log ) {
        logger.warn("user: " + principal.getName() + ": "  + log);
    }

}
