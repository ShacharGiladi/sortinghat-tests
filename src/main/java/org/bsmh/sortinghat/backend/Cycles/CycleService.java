package org.bsmh.sortinghat.backend.Cycles;

import javassist.NotFoundException;
import org.bsmh.sortinghat.backend.Loggers.RequestLogger;
import org.bsmh.sortinghat.backend.Tests.Test;
import org.bsmh.sortinghat.backend.Tests.TestRepository;
import org.bsmh.sortinghat.backend.Users.Models.User;
import org.bsmh.sortinghat.backend.Users.Repositorys.UserRepository;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class CycleService {
    @Autowired private CycleRepository cycleRepository;
    @Autowired private TestRepository testRepository;
    @Autowired private UserRepository userRepository;

    protected RequestLogger logger = new RequestLogger(LoggerFactory.getLogger(this.getClass()));

    public Cycle getById(Long cycleId) throws NotFoundException {

        Optional<Cycle> optionalCycle = cycleRepository.findById(cycleId);
        if(optionalCycle.isPresent()) {
            return optionalCycle.get();
        } else {
            throw new NotFoundException("Cycle was not found");
        }
    }

    public List<Cycle> getAllActiveCycles(Principal principal) {
        this.logger.debug(principal,"Returning all of the active cycles");
        return cycleRepository.getAllActiveCycles();
    }

    public Set<User> getAllUsersInCycle(long cycleId, Principal principal) {
        this.logger.debug(principal,"Returning all of the Users in cycle for cycle= " + cycleId);
        return cycleRepository.getOne(cycleId).getUsersInCycle();
    }

    public List<Test> getTestsInCycle(Long cycleId, Principal principal) {
        this.logger.debug(principal, "Returning all of the tests in cycle for cycle=" + cycleId);
        return cycleRepository.getOne(cycleId).getTestsInCycle();
    }

    public void setTestForCycle(Long cycleId, List<Test> tests, Principal principal) {
        this.logger.debug(principal, "Setting tests for the specific cycle= " + cycleId);
        cycleRepository.getOne(cycleId).setTestsInCycle(tests);
    }

    public void setUsersForCycle(Long cycleId, Set<User> users, Principal principal) {
        this.logger.debug(principal,"Setting users for the specific cycle= "+ cycleId);
        cycleRepository.getOne(cycleId).setUsersInCycle(users);
    }

    public void addTestToCycle(Long cycleId, Long testId, Principal principal) {
        this.logger.debug(principal,"Getting all of the tests in cycle for cycle= " + cycleId);
        this.logger.info(principal, "cycle : " + cycleRepository.findById(cycleId).get());
        Cycle cycle = cycleRepository.findById(cycleId).get();
        Test test = testRepository.findById(testId).get();
        cycle.addTestInCycle(test);
        cycleRepository.save(cycle);
        this.logger.debug(principal,"Added test - " + testId + " to cycle - " + cycleId);
    }

    public void addUserToCycle(Long cycleId, String userId, Principal principal) {
        this.logger.debug(principal, "Attempting to add user" + userId + "to the specific cycle= " + cycleId);
        Cycle cycle = cycleRepository.findById(cycleId).get();
        User user = userRepository.findById(userId).get();
        cycle.addUserInCycle(user);
        cycleRepository.save(cycle);
        this.logger.debug(principal, "Added user" + userId + "to the specific cycle= " + cycleId);
    }

    public JpaRepository<Cycle, Long> cycleRepository() {
        return cycleRepository;
    }
}
