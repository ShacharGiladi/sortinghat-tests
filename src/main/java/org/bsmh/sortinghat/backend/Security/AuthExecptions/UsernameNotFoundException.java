package org.bsmh.sortinghat.backend.Security.AuthExecptions;

public class UsernameNotFoundException extends Exception {
    public UsernameNotFoundException(String message) {
        super(message);
    }
}
