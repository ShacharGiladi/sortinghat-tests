package org.bsmh.sortinghat.backend.Questions;

import org.bsmh.sortinghat.backend.Questions.DTO.QuestionManagementDTO;
import org.bsmh.sortinghat.backend.Questions.Models.Case;
import org.bsmh.sortinghat.backend.Questions.Models.InputToCase;
import org.bsmh.sortinghat.backend.Questions.Models.OutputToCase;
import org.bsmh.sortinghat.backend.Questions.Models.Question;
import org.bsmh.sortinghat.backend.Questions.Repositorys.CaseRepository;
import org.bsmh.sortinghat.backend.Questions.Repositorys.InputToCaseRepository;
import org.bsmh.sortinghat.backend.Questions.Repositorys.OutputToCaseRepository;
import org.bsmh.sortinghat.backend.Questions.Repositorys.QuestionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class QuestionService {

    @Autowired
    private QuestionRepository questionRepository;

    @Autowired
    private CaseRepository caseRepository;

    @Autowired
    private InputToCaseRepository inputRepository;

    @Autowired
    private OutputToCaseRepository outputRepository;

    public List<QuestionManagementDTO> getQuestions() {
        List<QuestionManagementDTO> questions = (this.questionRepository.findAll().stream().map(q -> new QuestionManagementDTO(q)).collect(Collectors.toList()));
        for (QuestionManagementDTO q:questions) {
            List<Case> cases = new ArrayList<Case>();
            for (Case c:q.cases) {
                if(!c.isDisabled()) {
                    cases.add(c);
                }
            }
            q.cases = cases;
        }
        return questions;
    }

    public QuestionManagementDTO getQuestionById(Long questionId) {
        QuestionManagementDTO question = new QuestionManagementDTO(this.questionRepository.findById(questionId).get());
        question.cases = question.cases.stream().filter(c->!c.isDisabled()).collect(Collectors.toList());

        return question;
    }
    public List<Case> getAllCasesInQuestion(Long questionId) {
        return (this.questionRepository.findById(questionId).get().getCases().stream().filter(c->!c.isDisabled()).collect(Collectors.toList()));
    }

    public Case getCaseById(Long id) {
        return this.caseRepository.findById(id).get();
    }

    public void addQuestion(Question questionToAdd) {
        questionRepository.save(questionToAdd);
    }

    public void addCases(Long questionId, List<Case> cases) {
        // option to add multiple cases at once
    }



    public void addCaseToQuestion(Long questionId, Case caseToAdd) {
        Question questionToUpdate;
        questionToUpdate = this.questionRepository.findById(questionId).get();
        Case newCase = new Case();
        newCase.setQuestionId(caseToAdd.getQuestionId());
        this.caseRepository.save(newCase);
        for (InputToCase input : caseToAdd.getCaseInputsAsObject()) {
            input.setInput(input.getInput());
            input.setOrderOfInput((long)caseToAdd.getCaseInputsAsObject().indexOf(input) + 1);
            input.setCaseId(newCase.getCaseId());
            this.inputRepository.save(input);
        }

        for (OutputToCase output : caseToAdd.getCaseOutputsAsObject()) {
            output.setOutput(output.getOutput());
            output.setOrderOfOutput((long)caseToAdd.getCaseOutputsAsObject().indexOf(output) + 1);
            output.setCaseId(newCase.getCaseId());
            this.outputRepository.save(output);
        }

        newCase.setDisabled(false);
        this.caseRepository.save(newCase);

        double size = questionToUpdate.getCases().size();
        int afterDecimal = 2;
        double weight = Math.floor((1/size)*Math.pow(10,afterDecimal))/Math.pow(10,afterDecimal);

        if(weight * size == 1){
            for (Case currCase:questionToUpdate.getCases()) {
                currCase.setWeight(weight);
                caseRepository.save(currCase);
            }
        } else {
            for (int index = 0; index<size - 1; index++) {
                questionToUpdate.getCases().get(index).setWeight(weight);
                this.caseRepository.save(questionToUpdate.getCases().get(index));
            }
            questionToUpdate.getCases().get((int)size - 1).setWeight((Math.round((1-((size - 1) * weight))*Math.pow(10,afterDecimal))/Math.pow(10,afterDecimal)));
            this.caseRepository.save(questionToUpdate.getCases().get((int)size - 1));
        }

        this.caseRepository.save(newCase);
        this.questionRepository.save(questionToUpdate);
    }

    public void deleteCaseOfQuestion(Long questionId, Long caseToDeleteId) {
        Question questionToUpdate;
        questionToUpdate = questionRepository.findById(questionId).get();
        Case caseToDelete = this.caseRepository.findById(caseToDeleteId).get();
        caseToDelete.setDisabled(true);

        double size = questionToUpdate.getCases().size() - 1;
        int afterDecimal = 2;

        if (size != 0) {
            double weight = Math.floor((1/size)*Math.pow(10,afterDecimal))/Math.pow(10,afterDecimal);
            if(weight * size == 1){
                for (Case currCase:questionToUpdate.getCases()) {
                    currCase.setWeight(weight);
                    this.caseRepository.save(currCase);
                }
            } else {
                for (int index = 0; index<size - 1; index++) {
                    questionToUpdate.getCases().get(index).setWeight(weight);
                    this.caseRepository.save(questionToUpdate.getCases().get(index));
                }

                questionToUpdate.getCases().get((int)size - 1).setWeight((Math.round((1-((size - 1) * weight))*Math.pow(10,afterDecimal))/Math.pow(10,afterDecimal)));
                this.caseRepository.save(questionToUpdate.getCases().get((int)size - 1));
            }
        }

        this.questionRepository.save(questionToUpdate);
    }

    public void deleteCasesOfQuestion(Long questionId, Long[] casesToDeleteId) {
        for (int index = 0; index < casesToDeleteId.length; index++) {
            this.deleteCaseOfQuestion(questionId,casesToDeleteId[index].longValue());
        }
    }


    public void changeDescription(Question newQuestion) {
        questionRepository.findById(newQuestion.getId()).get().setDescription(newQuestion.getDescription());
        this.questionRepository.save(questionRepository.findById(newQuestion.getId()).get());
    }
}
