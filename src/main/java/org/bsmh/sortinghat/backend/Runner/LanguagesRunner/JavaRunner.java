package org.bsmh.sortinghat.backend.Runner.LanguagesRunner;


import org.bsmh.sortinghat.backend.Runner.Exceptions.EvilCodeException;

import org.springframework.beans.factory.annotation.Value;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class JavaRunner extends CodeRunner {

    @Value("${runner.java}")
    private String javaCommand;

    @Override
    public List<String> run(String fileLocation, List<String> processInputs, String userId) throws Exception {
        super.setCodeRunner(this.javaCommand);
        return super.run(fileLocation, processInputs, userId);
    }

    @Override
    public void checkIfCodeIsEvil(String code) throws  EvilCodeException {
        String[] keywords = new String[] {"Base64", "cmd", "CMD", "processBuilder", "Process", "Socket", "net",
                "FileSystem", "ScriptEngine", "File"};

        super.checkIfCodeIsEvil(code, Arrays.asList(keywords));
    }
}
