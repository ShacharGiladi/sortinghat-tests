package org.bsmh.sortinghat.backend.Tests;

import org.bsmh.sortinghat.backend.EntityController;
import org.bsmh.sortinghat.backend.Questions.Models.Case;
import org.bsmh.sortinghat.backend.Questions.Models.Question;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/tests")
public class TestController extends EntityController<Test, Long> {

    @Autowired
    private TestRepository testRepository;

    @GetMapping("/{testId}/questions")
    public List<Question> getQuestionInTest(@PathVariable Long testId, Principal principal) {
        this.logger.info(principal,"get all tests questions for tests " + testId);
        return testRepository.getOne(testId).getQuestions().stream().filter(q -> q != null).collect(Collectors.toList());
    }

    @PostMapping("/{testId}/questions")
    public String addQuestionsToTest(@PathVariable Long testId, @RequestBody List<Question> questions, Principal principal) {
        this.logger.info(principal,"adding question for questions for tests " + testId + "questions : " + questions);
        Test test = this.getRepository().findById(testId).get();
        test.addQuestions(questions);
        this.getRepository().save(test);
        return "";
    }

    @PostMapping("/{testId}/question")
    public String addQuestionsToTest(@PathVariable Long testId, @RequestBody Question question, Principal principal) {
        this.logger.info(principal,"adding question for questions for tests " + testId + "question : " + question);

        Test test = this.getRepository().findById(testId).get();
        question.setCases(new ArrayList<Case>());
        test.addQuestion(question);
        this.getRepository().save(test);
        return "";
    }

    @Override
    protected JpaRepository<Test, Long> getRepository() {
        return this.testRepository;
    }
}
