package org.bsmh.sortinghat.backend;

import org.bsmh.sortinghat.backend.Loggers.RequestLogger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

public abstract class EntityController<E, I> {
    protected abstract JpaRepository<E, I> getRepository();

    protected RequestLogger logger;

    public EntityController() {
        this.logger = new RequestLogger(LoggerFactory.getLogger(this.getClass()));
    }

    @GetMapping("")
    public List<E> queryAll(Principal principal) {
        this.logger.info(principal, "requested all");
        return this.getRepository().findAll();
    }

    @GetMapping("/{id}")
    public E getById(@PathVariable I id, Principal principal) {
        this.logger.info(principal, "get by id request, id = " + id);
        return this.getRepository().findById(id).orElseThrow();
    }

    @PostMapping("")
    @ResponseStatus(HttpStatus.CREATED)
    public E create(@RequestBody E entity, Principal principal) {
        this.logger.info(principal, " create request, entity = " + entity.toString());
        return this.getRepository().save(entity);
    }

    @PutMapping("")
    public E update(@RequestBody E entity, Principal principal) {
        this.logger.info(principal, " update request, entity = " + entity.toString());
        return this.getRepository().save(entity);
    }
}
