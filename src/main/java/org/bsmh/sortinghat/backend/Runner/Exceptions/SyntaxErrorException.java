package org.bsmh.sortinghat.backend.Runner.Exceptions;

import java.util.List;

public class SyntaxErrorException extends Exception {

    private List<String> errorList;

    public SyntaxErrorException(List<String> errorList) {
        this.errorList = errorList;
    }

    public List<String> getErrorList() {
        return errorList;
    }
}
