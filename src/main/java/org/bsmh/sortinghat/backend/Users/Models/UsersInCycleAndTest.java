package org.bsmh.sortinghat.backend.Users.Models;

import org.bsmh.sortinghat.backend.Cycles.Cycle;
import org.bsmh.sortinghat.backend.Tests.Test;

import javax.persistence.*;

@Entity
@Table(name = "student_in_cycle")
public class UsersInCycleAndTest {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @ManyToOne
    @JoinColumn(name = "cycle_id")
    private Cycle cycle;

    @ManyToOne
    @JoinColumn(name = "test_id")
    private Test test;

    public UsersInCycleAndTest() {
    }

    public UsersInCycleAndTest(User user, Cycle cycle, Test test) {
        this.user = user;
        this.cycle = cycle;
        this.test = test;
    }

    public Long getId() {
        return id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Cycle getCycle() {
        return cycle;
    }

    public void setCycle(Cycle cycle) {
        this.cycle = cycle;
    }

    public Test getTest() {
        return test;
    }

    public void setTest(Test test) {
        this.test = test;
    }
}
