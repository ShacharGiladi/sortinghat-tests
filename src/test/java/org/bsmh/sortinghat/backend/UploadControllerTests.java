package org.bsmh.sortinghat.backend;

import org.junit.Assert;
import org.junit.Test;
import org.junit.internal.runners.statements.Fail;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMultipartHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;



import java.io.File;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;

import static org.junit.Assert.fail;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class UploadControllerTests {

//    @Value("${Upload.directory}")
//    private String homeDir;
//
//    @Autowired
//   private MockMvc mockMvc;
//
//
//    public void testUploadFile(String textToWrite) {
//        MockMultipartFile fileUpload = new MockMultipartFile("src", "omer.txt", "text/plain", textToWrite.getBytes());
//        MockHttpServletRequestBuilder  builder =  MockMvcRequestBuilders.multipart("/api/Upload").file(fileUpload);
//        try {
//            mockMvc.perform(builder)
//            .andExpect(status().isOk());
//            File file = new File (Paths.get(homeDir, "omer.txt").toString());
//
//            Assert.assertTrue(file.isFile());
//            Assert.assertArrayEquals(textToWrite.getBytes(), Files.readAllBytes(file.toPath()));
//        } catch (Exceptions e) {
//            fail(e.getMessage());
//        }
//    }
//
//    @Test
//    public void testUploadOnce() {
//        this.testUploadFile("hello1");
//    }
//
//    @Test
//    public void testUploadTwice() {
//        this.testUploadFile("hello1");
//        this.testUploadFile("hello2");
//    }
}
