package org.bsmh.sortinghat.backend.Runner.Exceptions;

import java.util.List;

public class EvilCodeException extends Exception {
    private List<String> errorList;

    public EvilCodeException(List<String> errorList) {
        super(String.join("\n", errorList));
        this.errorList = errorList;
    }

    public List<String> getErrorList() {
        return errorList;
    }
}
