package org.bsmh.sortinghat.backend.Questions.Repositorys;

import org.bsmh.sortinghat.backend.Questions.Models.OutputToCase;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OutputToCaseRepository extends JpaRepository<OutputToCase, Long> {
}
