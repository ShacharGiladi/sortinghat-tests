package org.bsmh.sortinghat.backend.Users.Exceptions;

public class DoubleStartException extends Exception {
    public DoubleStartException() {
        super("user started test twice");
    }
}
