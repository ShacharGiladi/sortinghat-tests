package org.bsmh.sortinghat.backend.Questions.Models;

import org.bsmh.sortinghat.backend.Tests.Test;

import javax.persistence.*;

@Entity
@Table(name = "questions_in_test")
public class QuestionInTest {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "test_id")
    private Test test;

    @ManyToOne
    @JoinColumn(name = "question_id")
    private Question question;

    @Column(name = "order_of_question")
    private int orderOfQuestion;

    @Column(name = "weight_of_question")
    private double weightOfQuestion;

    public QuestionInTest() {
    }

    public QuestionInTest(Test test, Question question, Double weightOfQuestion, Integer orderOfQuestion) {
        this.test = test;
        this.question = question;
        this.weightOfQuestion = weightOfQuestion;
        this.orderOfQuestion = orderOfQuestion;
    }

    public Test getTest() {
        return test;
    }

    public void setTest(Test test) {
        this.test = test;
    }

    public Question getQuestion() {
        return question;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }

    public double getWeightOfQuestion() {
        return weightOfQuestion;
    }

    public void setWeightOfQuestion(double weightOfQuestion) {
        this.weightOfQuestion = weightOfQuestion;
    }

    public int getOrderOfQuestion() {
        return orderOfQuestion;
    }

    public void setOrderOfQuestion(int weightOfQuestion) {
        this.orderOfQuestion = weightOfQuestion;
    }
}
