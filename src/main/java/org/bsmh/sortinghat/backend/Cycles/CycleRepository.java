package org.bsmh.sortinghat.backend.Cycles;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface CycleRepository extends JpaRepository<Cycle, Long> {
    List<Cycle> findByEndDateIsAfter(Date date);

    default List<Cycle> getAllActiveCycles(){
        return findByEndDateIsAfter(new Date());
    }

}
