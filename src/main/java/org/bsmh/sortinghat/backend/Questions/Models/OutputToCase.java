package org.bsmh.sortinghat.backend.Questions.Models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "output_in_case")
public class OutputToCase {
    @Id
    @Column(name = "output_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long outputId;

    @Column(name = "output")
    private String output;

    @Column(name = "case_id")
    private Long caseId;

    @Column(name = "order_of_output")
    private Long orderOfOutput;

    public Long getCaseId() {
        return caseId;
    }

    public void setCaseId(Long caseId) {
        this.caseId = caseId;
    }

    public Long getOrderOfOutput() {
        return orderOfOutput;
    }

    public void setOrderOfOutput(Long orderOfOutput) {
        this.orderOfOutput = orderOfOutput;
    }

    public String getOutput() {
        return output;
    }

    public void setOutput(String output) {
        this.output = output;
    }

    public Long getOutputId() {
        return outputId;
    }

    public void setOutputId(Long outputId) {
        this.outputId = outputId;
    }

    @Override
    public String toString() {
        return "OutputToCase{" +
                "outputId=" + outputId +
                ", output='" + output + '\'' +
                '}';
    }
}
