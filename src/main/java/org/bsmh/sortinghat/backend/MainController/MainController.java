package org.bsmh.sortinghat.backend.MainController;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@RestController
public class MainController {
    @GetMapping("/")
    public void redirect(HttpServletResponse httpResponse) throws IOException {
        httpResponse.sendRedirect("/public/index.html");

    }
}
