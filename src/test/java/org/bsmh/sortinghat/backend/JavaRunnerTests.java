package org.bsmh.sortinghat.backend;

import org.bsmh.sortinghat.backend.Runner.Exceptions.ErrorInProcessException;
import org.bsmh.sortinghat.backend.Runner.Exceptions.EvilCodeException;
import org.bsmh.sortinghat.backend.Runner.Exceptions.InfiniteLoopException;
import org.bsmh.sortinghat.backend.Runner.LanguagesRunner.JavaRunner;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class JavaRunnerTests {
    @Autowired
    JavaRunner javaRunner;

//TODO: Complete testing

    @Test(expected = EvilCodeException.class)
    public void check_code_evail() throws Exception {
        String code = " import java.io.File; \npublic class main {" +
                "\n" +
                "public static void main(String[] args) { \n" +
                "while(true) {}" +
                "\n } \n}";
        javaRunner.checkIfCodeIsEvil(code);

    }

    private Path createJavaFile(String code) throws IOException {
        Path path = Paths.get("FileUploads", "w951501308", "TestFile.java");
        Files.createDirectories(path.getParent());
        byte[] data =code.getBytes();
        Files.write(path, data);
        return path.toAbsolutePath();
    }
}
