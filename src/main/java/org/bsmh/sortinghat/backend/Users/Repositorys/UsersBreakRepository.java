package org.bsmh.sortinghat.backend.Users.Repositorys;

import org.bsmh.sortinghat.backend.Users.Models.UsersBreaks;
import org.bsmh.sortinghat.backend.Users.Models.UsersTimes;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UsersBreakRepository extends JpaRepository<UsersBreaks, Long> {
}
