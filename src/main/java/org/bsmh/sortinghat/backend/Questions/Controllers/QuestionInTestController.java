package org.bsmh.sortinghat.backend.Questions.Controllers;

import org.bsmh.sortinghat.backend.EntityController;
import org.bsmh.sortinghat.backend.Questions.Models.Question;
import org.bsmh.sortinghat.backend.Questions.Models.QuestionInTest;
import org.bsmh.sortinghat.backend.Questions.Repositorys.QuestionInTestRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/api/testquestions")
public class QuestionInTestController extends EntityController<QuestionInTest, Long> {
    @Autowired
    QuestionInTestRepository questionInTestRepository;

    @GetMapping("/test/{id}")
    public List<QuestionInTest> getByTestId(@PathVariable Long id, Principal principal) {
        return this.questionInTestRepository.findByTest_id(id);
    }

    @Override
    protected JpaRepository<QuestionInTest, Long> getRepository() {
        return questionInTestRepository;
    }
}
