package org.bsmh.sortinghat.backend.Users.Services;

import org.bsmh.sortinghat.backend.Cycles.Cycle;
import org.bsmh.sortinghat.backend.Cycles.CycleRepository;
import org.bsmh.sortinghat.backend.Security.AuthExecptions.UnauthorizedException;
import org.bsmh.sortinghat.backend.Security.JwtTokenProvider;
import org.bsmh.sortinghat.backend.Tests.Test;
import org.bsmh.sortinghat.backend.Tests.TestRepository;
import org.bsmh.sortinghat.backend.Users.DTOS.FullUserInfoDTO;
import org.bsmh.sortinghat.backend.Users.DTOS.UsersInCycleInTestDTO;
import org.bsmh.sortinghat.backend.Users.Exceptions.*;
import org.bsmh.sortinghat.backend.Users.Models.User;
import org.bsmh.sortinghat.backend.Users.Models.UsersBreaks;
import org.bsmh.sortinghat.backend.Users.Models.UsersInCycleAndTest;
import org.bsmh.sortinghat.backend.Users.Models.UsersTimes;
import org.bsmh.sortinghat.backend.Users.Repositorys.UserRepository;
import org.bsmh.sortinghat.backend.Users.Repositorys.UsersBreakRepository;
import org.bsmh.sortinghat.backend.Users.Repositorys.UsersCycleAndTestRepository;
import org.bsmh.sortinghat.backend.Users.Repositorys.UsersTimesRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.NullValueInNestedPathException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.support.NullValue;
import org.springframework.http.HttpStatus;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class UserService {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private JwtTokenProvider jwtTokenProvider;

    @Autowired
    private UsersTimesRepository usersTimesRepository;

    @Autowired
    private UsersBreakRepository usersBreakRepository;

    @Autowired
    private UsersCycleAndTestRepository usersCycleAndTestRepository;

    @Autowired
    private CycleRepository cycleRepository;

    @Autowired
    UsersCycleAndTestRepository usersInCycleAndTestRepository;

    @Autowired TestRepository testRepository;

    Logger logger = LoggerFactory.getLogger(this.getClass());

    public String signIn(String userId, String password) {
        try {
            logger.info("user id : " + userId + " try to logged in with password " + password);
            Optional<User> user = userRepository.findByIdAndPassword(userId, password);
            if(user.isPresent()) {
                logger.info("user id : " + userId + " was found");
                return jwtTokenProvider.createToken(userId, user.get().getRole());
            } else {
                logger.info("user id : " + userId + " was not found with password: " + password);
                throw new UnauthorizedException("Invalid username/password supplied", HttpStatus.UNPROCESSABLE_ENTITY);
            }

        } catch (AuthenticationException e) {
            throw new UnauthorizedException("Invalid username/password supplied", HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    public User getUserFromToken(HttpServletRequest req) {
        return userRepository.findById(jwtTokenProvider.getUsername(jwtTokenProvider.resolveToken(req))).get();
    }

    public List<UsersInCycleAndTest> getUsersCyclesAndTests(String userId) {
            return usersInCycleAndTestRepository.findByUser_Id(userId);
    }

    public void createNewUsersCycleAndTest(UsersInCycleAndTest newUsersInCycleAndTest) {
        UsersInCycleAndTest assignableUsersCycleAndTest = new UsersInCycleAndTest(newUsersInCycleAndTest.getUser(), newUsersInCycleAndTest.getCycle(), newUsersInCycleAndTest.getTest());
        usersInCycleAndTestRepository.save(assignableUsersCycleAndTest);
    }

    public void deleteUsersCycleAndTest(Long usersInCycleAndTestId) {
        usersInCycleAndTestRepository.deleteById(usersInCycleAndTestId);
    }

    public FullUserInfoDTO getFullUserInfo(String userId) {
        return new FullUserInfoDTO(userRepository.findById(userId).get());
    }

    public List<FullUserInfoDTO>getAllUsersFullInfo() {
        return userRepository.findAll().stream().map(c -> new FullUserInfoDTO(c)).collect(Collectors.toList());
    }

    public void startUserTime(String userId, Long testId, Long cycleId) throws DoubleStartException {
        if (usersTimesRepository.findByUserIdAndCycleIdAndTestId(userId, cycleId, testId).isPresent()) {
            throw new DoubleStartException();
        }

        UsersTimes usersTimes = new UsersTimes(userId, testId, cycleId);

        Date dateOfStart = new Date();
        usersTimes.setStartDate(dateOfStart);
        int testTime = testRepository.getOne(testId).getTime();
        Date dateOfEnd = new Date(dateOfStart.getTime() + testTime * 60000);
        usersTimes.setDateOfEnd(dateOfEnd);

        usersTimesRepository.save(usersTimes);
    }

    public void addTimeToUser(Integer timeToAddInMin, String userId, Long testId, Long cycleId) throws UserDidntStartTestException {
        Optional<UsersTimes> usersTime = usersTimesRepository.findByUserIdAndCycleIdAndTestId(userId, cycleId, testId);
        if(!usersTime.isPresent()) {
            throw new UserDidntStartTestException();
        }

        Date newEndDate = new Date(usersTime.get().getDateOfEnd().getTime() + timeToAddInMin * 60000);
        usersTime.get().setDateOfEnd(newEndDate);
        usersTimesRepository.save(usersTime.get());
    }

    public User giveUserBreak(String userId, Long testId, Long cycleId) throws UserWasNotFoundException, UserIsAllReadyOnBreakException, UserDidntStartTestException {
        User user = userBreakValidation(userId, testId, cycleId);

        if (user.getIsUserInBreak()) {
            throw new UserIsAllReadyOnBreakException(userId);
        }


        UsersBreaks usersBreak = new UsersBreaks(userId, testId, cycleId);

        usersBreakRepository.save(usersBreak);

        user.getUsersBreaks().add(usersBreak);

        return user;
    }

    private User userBreakValidation(String userId, Long testId, Long cycleId) throws UserDidntStartTestException, UserWasNotFoundException {
        Optional<User> user = userRepository.findById(userId);
        Optional<UsersTimes> usersTime = usersTimesRepository.findByUserIdAndCycleIdAndTestId(userId, cycleId, testId);

        if (!user.isPresent()) {
            throw new UserDidntStartTestException();
        }

        if (!user.isPresent()) {
            throw new UserWasNotFoundException(userId);
        }

        return user.get();
    }

    public User returnUserFromBreak(String userId, Long cycleId, Long testId) throws UserWasNotFoundException, UserIsNotInBreakException, UserDidntStartTestException {
        User user = userBreakValidation(userId, testId, cycleId);

        if (!user.getIsUserInBreak()) {
            throw new UserIsNotInBreakException(userId);
        }

        for (UsersBreaks usersBreaks : user.getUsersBreaks()) {
            Optional<UsersTimes> usersTime = usersTimesRepository.findByUserIdAndCycleIdAndTestId(userId, cycleId, testId);

            if (!usersBreaks.getDidReturn()) {
                usersBreaks.setDidReturn(true);
                usersBreaks.setDateOfEnd(new Date());
                usersBreakRepository.save(usersBreaks);
                UsersTimes currUsersTime = getUserCurrentTime(cycleId, testId, user.getUsersTimes());
                Date newDateOfEnd = new Date(new Date().getTime() + currUsersTime.getDateOfEnd().getTime() - usersBreaks.getDateOfExit().getTime());
                usersTime.get().setDateOfEnd(newDateOfEnd);
                usersTimesRepository.save(usersTime.get());

                break;
            }
        }

        return user;
    }

    public List<User> createUsers(List<User> userList) throws Exception {
        this.logger.info( "start to create users");
        ArrayList<User> toRemove = new ArrayList<>();

       for (User currUser : userList) {
            if(currUser.getId() == null || currUser.getUsername() == null) {
                throw new Exception();
            }

            currUser.setPassword(currUser.getId());
            if(userRepository.findById(currUser.getId()).isPresent()) {
                toRemove.add(currUser);

            }
        }
        userList.removeAll(toRemove);
        userRepository.saveAll(userList);
        this.logger.info( "finished to create");

        return toRemove;
    }

    @Transactional
    public List<User> addUsersToCycle(Long cycleId, Long testId, List<User> userList) throws Exception {

        List<User> usersAlerdayOpen = createUsers(userList);

        this.logger.info(" add to cycle");
        List<UsersInCycleAndTest> usersInCycleAndTests = new ArrayList<>();
        Cycle cycleToAdd = cycleRepository.findById(cycleId).get();
        Test testToAdd = testRepository.findById(testId).get();

        for(User user : userList) {
            usersInCycleAndTests.add(new UsersInCycleAndTest(user, cycleToAdd, testToAdd));
        }

        usersCycleAndTestRepository.saveAll(usersInCycleAndTests);

        this.logger.info( " finished");

         return userList;
    }

    @Transactional
    public void addToCycleIfNotInCycle(Long cycleId, Long testId, List<User> userList) {
        List<UsersInCycleAndTest> usersInCycleAndTests = new ArrayList<>();
        Cycle cycleToAdd = cycleRepository.findById(cycleId).get();
        Test testToAdd = testRepository.findById(testId).get();
        for(User currUser : userList) {
            Optional<User> userFromDb = userRepository.findById(currUser.getId());
            if(userFromDb.isPresent()) {
                boolean isEsist = this.findIfUserInCycleAndTest(userFromDb.get(), cycleId, testId);
                if(!isEsist) {
                    usersInCycleAndTests.add(new UsersInCycleAndTest(userFromDb.get(), cycleToAdd, testToAdd));
                }
            }

        }

        usersCycleAndTestRepository.saveAll(usersInCycleAndTests);

    }

    public List<UsersInCycleInTestDTO> getAllTestsAndCycle() {
        return usersInCycleAndTestRepository.findAll().stream().map(c -> new UsersInCycleInTestDTO(c)).collect(Collectors.toList());
    }

    private boolean findIfUserInCycleAndTest(User user, Long cycleId, Long testId) {
        List<UsersInCycleAndTest> usersInCycleAndTests = user.getUsersInCycleInTest();
        for(UsersInCycleAndTest currCycleAndTest : usersInCycleAndTests) {
            if(currCycleAndTest.getCycle().getCycleId() == cycleId && currCycleAndTest.getTest().getId() == testId) {
                return true;
            }
        }

        return false;
    }



    private UsersTimes getUserCurrentTime(Long cycleId, Long testId, List<UsersTimes> usersTimes) throws NoSuchElementException {
        for(UsersTimes currUsersTime : usersTimes) {
            if(currUsersTime.getCycleId() == cycleId && currUsersTime.getTest().getId() == testId) {
                return currUsersTime;
            }
        }
        throw new NoSuchElementException();
    }

    public void editUser(FullUserInfoDTO userInfo) {
        User newUser = userRepository.findById(userInfo.id).get();
        newUser.matchToFullUserInfoDTO(userInfo);
        userRepository.save(newUser);
    }

}
