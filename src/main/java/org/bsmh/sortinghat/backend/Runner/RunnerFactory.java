package org.bsmh.sortinghat.backend.Runner;

import org.bsmh.sortinghat.backend.Runner.LanguagesRunner.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

public class RunnerFactory {

    @Autowired
    private ApplicationContext context;

    public CodeRunner getRunner(String runner) {
        if(runner.equals("cs")) {
            return context.getBean(CsRunner.class);
        } else if(runner.equals("java")) {
            return context.getBean(JavaRunner.class);
        } else if (runner.equals("py")) {
            return context.getBean(PythonRunner.class);
        } else if(runner.equals("cpp")) {
            return context.getBean(CppRunner.class);
        } else if(runner.equals("c")) {
            return context.getBean(CppRunner.class);
        }

        return null;
    }

    public Boolean isNeedToCompile(String runner) {
        if(runner.equals("cs") || runner.equals("cpp") || runner.equals("c")) {
            return true;
        } else {
            return false;
        }
    }

}
