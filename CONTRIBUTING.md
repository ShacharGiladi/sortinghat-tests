# Contributing to the Sorting Hat

Hello and welcome! :wave:

## Style guide

### Java

Use google's java formatter before commiting a file, read about it [here](https://github.com/google/google-java-format)
its best to set it up as a pre-commit hook