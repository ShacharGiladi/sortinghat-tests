package org.bsmh.sortinghat.backend.Runner.LanguagesRunner;

import org.bsmh.sortinghat.backend.Runner.Exceptions.EvilCodeException;

import org.springframework.beans.factory.annotation.Value;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class CsRunner extends CodeWithCompileRunner {
    @Value("${runner.cs.compiler}")
    private String csCompiler;

    @Override
    public List<String> run(String fileLocation, List<String> processInputs, String userId) throws Exception {
        return super.run(fileLocation, processInputs, userId);
    }

    @Override
    public String compileCode(String fileLocation, String userId) throws Exception {
        this.setCompilerCommand(csCompiler);
        return super.compileCode(fileLocation, userId);
    }

    @Override
    public void checkIfCodeIsEvil(String code) throws EvilCodeException {
        String[] keywords = new String[] {"Base64", "cmd", "CMD", "Diagnostics", "Process","OperatingSystem", "Socket", "Net",
                "Socket", "FileSystem", "User32", "Eval"};

        super.checkIfCodeIsEvil(code, Arrays.asList(keywords));
    }
}
