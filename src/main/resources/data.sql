 INSERT INTO users (id, username, password, role_id, finished_interview ) VALUES
    ('209160100', 'איתמר לחוביצר', 'pass', 1, FALSE),
   	('2', 'lolo', 'lala', 4, FALSE);

  INSERT INTO questions ( description, is_demo, question_name) VALUES
      ('קלוט מספר והדפס את המספר פלוס 1', TRUE, 'שאלה ראשונה'),
      ( 'קלוט 2 מספרים והדפס את שניהם + 1', FALSE,  'שאלה שנייה'),
	  ( 'קלוט 3 מספרים והדפס בשורה נפרדת כל אחד מהם כפול 2', FALSE,  'שאלה שלישית');

INSERT INTO cases (weight, question_id) VALUES
      ( 0.5, 1),
      ( 0.5, 1),
	  (1, 2),
	  (0.3, 3),
	  (0.3, 3),
	  (0.4, 3);
	 
  INSERT INTO input_in_case (order_of_input, input, case_id) VALUES
      (1, '1', 1),
      (1, '2', 2),
      (1, '1', 3),
      (2, '2', 3),
	  (1, '1', 4),
	  (2, '2', 4),
	  (3, '3', 4),
	  (1, '4', 5),
	  (2, '5', 5),
	  (3, '6', 5),
	  (1, '7', 6),
	  (2, '8', 6),
	  (3, '9', 6);

 INSERT INTO output_in_case (order_of_output, output, case_id) VALUES
      (1,'2', 1),
      (1, '3', 2),
      (1, '2', 3),
      (2, '3', 3),
	  (1, '2', 4),
	  (2, '4', 4),
	  (3, '6', 4),
	  (1, '8', 5),
	  (2, '10', 5),
	  (3, '12', 5),
	  (1, '14', 6),
	  (2, '16', 6),
	  (3, '18', 6);
 
 INSERT INTO test (test_name) VALUES
      ('מבחן לקורס תכנות');
 
  INSERT INTO questions_in_test ( test_id, question_id, order_of_question, weight_of_question) VALUES
      (1, 1, 1,  0.3),
      (1, 2, 2,  0.3),
	  (1, 3, 3,  0.4);

  INSERT INTO cycle ( start_date, end_date, cycle_name) VALUES
      ('2019-06-16 ', ' 2019-10-10 ', 'מיון קיץ 2019');

  INSERT INTO tests_in_cycle (test_id, cycle_id) VALUES
      (1, 1);

  INSERT INTO student_in_cycle (user_id, cycle_id, test_id) VALUES
      ('209160100', 1, 1),
      ('209160100', 1, 2);
