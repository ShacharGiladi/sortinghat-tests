CREATE TABLE users
(
    id       text PRIMARY KEY,
    username text NOT NULL,
    password text NOT NULL,
	finished_interview BOOLEAN,
    role_id  INTEGER     NOT NULL
);

CREATE TABLE cycle
(
    cycle_id   serial PRIMARY KEY ,
    start_date DATE        NOT NULL,
    end_date   DATE        NOT NULL,
    cycle_name text NOT NULL
);


CREATE TABLE questions
(
    question_id   serial PRIMARY KEY,
    description   text NOT NULL,
    is_demo       BOOLEAN      NOT NULL,
    question_name text  NOT NULL
);

CREATE TABLE cases
(
    case_id     serial PRIMARY KEY ,
    weight      decimal  NOT NULL,
    question_id INTEGER NOT NULL,
    foreign key (question_id) references questions (question_id)
);
CREATE TABLE input_in_case
(
    input_id       serial PRIMARY KEY ,
    order_of_input INTEGER     NOT NULL,
    input          text NOT NULL,
    case_id        INTEGER     NOT NULL,
    foreign key (case_id) references cases (case_id)
);



CREATE TABLE output_in_case
(
    output_id       serial PRIMARY KEY,
    order_of_output INTEGER     NOT NULL,
    output          text NOT NULL,
    case_id         INTEGER     NOT NULL,
    foreign key (case_id) references cases (case_id)
);



CREATE TABLE test
(
    test_id      serial PRIMARY KEY ,
    test_name    text NOT NULL,
    instructions text
);

CREATE TABLE users_break
(
    id             serial PRIMARY KEY ,
    user_id        text NOT NULL,
    date_of_exit   TIMESTAMP   NOT NULL,
    date_of_return TIMESTAMP,
    did_return     BOOLEAN,
    cycle_id       INTEGER     not null,
    test_id        INTEGER     not null,
    foreign key (user_id) references users (id),
    foreign key (test_id) references test (test_id),
    foreign key (cycle_id) references cycle (cycle_id)
);

CREATE TABLE user_tests_times
(
    id            serial PRIMARY KEY ,
    user_id       text NOT NULL,
    cycle_id      INTEGER     not null,
    test_id       INTEGER     not null,
    date_of_start TIMESTAMP   NOT NULL,
    date_of_end   TIMESTAMP   NOT NULL,
    foreign key (cycle_id) references cycle (cycle_id),
    foreign key (user_id) references users (id),
    foreign key (test_id) references test (test_id)
);



CREATE TABLE questions_in_test
(
    id          serial  PRIMARY KEY,
    test_id     INTEGER,
    question_id INTEGER,
	order_of_question INTEGER,
	weight_of_question decimal ,
    foreign key (test_id) references test (test_id),
    foreign key (question_id) references questions (question_id)
);

CREATE TABLE tests_in_cycle
(
    id serial PRIMARY KEY,
    test_id  INTEGER,
    cycle_id INTEGER,
    foreign key (test_id) references test (test_id),
    foreign key (cycle_id) references cycle (cycle_id)
);



CREATE TABLE student_in_cycle
(
    id serial PRIMARY KEY,
    user_id  text,
    cycle_id INTEGER,
    test_id INTEGER,
    foreign key (user_id) references users (id),
    foreign key (cycle_id) references cycle (cycle_id),
    foreign key (test_id) references  test (test_id)
);

CREATE TABLE answers_per_student_in_cycle
(
    id                 serial PRIMARY KEY,
    student_id         text,
    cycle_id           INTEGER,
    case_id            INTEGER,
    question_id        INTEGER,
    test_id            INTEGER,
    is_correct         BOOLEAN,
    description       text,
    foreign key (test_id) references test (test_id),
    foreign key (question_id) references questions (question_id),
    foreign key (case_id) references cases (case_id),
    foreign key (cycle_id) references cycle (cycle_id),
    foreign key (student_id) references users (id),
    weight_of_question decimal NOT NULL
);

CREATE TABLE users_code_submit
(
    id serial PRIMARY KEY,
    cycle_id INTEGER,
    test_id INTEGER,
    question_id INTEGER,
    student_id text,
    code text,
    language_of_code text,

    foreign key (cycle_id) references cycle (cycle_id),
    foreign key (test_id) references test (test_id),
    foreign key (student_id) references users (id),
    FOREIGN KEY (question_id) REFERENCES questions(question_id)
)