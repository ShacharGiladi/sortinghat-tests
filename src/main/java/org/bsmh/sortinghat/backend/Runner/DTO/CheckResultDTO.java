package org.bsmh.sortinghat.backend.Runner.DTO;

import org.bsmh.sortinghat.backend.Runner.Exceptions.ErrorInProcessException;
import org.bsmh.sortinghat.backend.Runner.Exceptions.SyntaxErrorException;

import java.util.ArrayList;
import java.util.List;

public class CheckResultDTO {
    public List<String> outputs;
    public List<String> errors;
    public List<String> syntaxErrors;
    public boolean infiniteLoop;

    public CheckResultDTO(List<String> outputs) {
        this.outputs = outputs;
        this.errors = new ArrayList<>();
        this.syntaxErrors = new ArrayList<>();
        this.infiniteLoop = false;
    }

    public CheckResultDTO(SyntaxErrorException ex) {
        this.outputs = new ArrayList<>();
        this.errors = new ArrayList<>();
        this.syntaxErrors = ex.getErrorList();
        this.infiniteLoop = false;
    }

    public CheckResultDTO(ErrorInProcessException ex) {
        this.outputs = new ArrayList<>();
        this.errors = ex.getErrors();
        this.syntaxErrors = new ArrayList<>();
        this.infiniteLoop = false;
    }

    public CheckResultDTO(boolean infiniteLoop) {
        this.infiniteLoop = infiniteLoop;
        this.outputs = new ArrayList<>();
        this.errors = new ArrayList<>();
        this.syntaxErrors = new ArrayList<>();
    }


    public List<String> getOutputs() {
        return outputs;
    }

    public void setOutputs(List<String> outputs) {
        this.outputs = outputs;
    }

    public List<String> getErrors() {
        return errors;
    }

    public void setErrors(List<String> errors) {
        this.errors = errors;
    }

    public List<String> getSyntaxErrors() {
        return syntaxErrors;
    }

    public void setSyntaxErrors(List<String> syntaxErrors) {
        this.syntaxErrors = syntaxErrors;
    }

    public CheckResultDTO(List<String> outputs, List<String> errors, List<String> syntaxErrors, boolean infiniteLoop) {
        this.outputs = outputs;
        this.errors = errors;
        this.syntaxErrors = syntaxErrors;
        this.infiniteLoop = infiniteLoop;
    }
}
