package org.bsmh.sortinghat.backend.Users.Exceptions;

public class UserIsNotInBreakException extends Exception {
    public UserIsNotInBreakException(String userId) {
        super("user " + userId + " is not on break");
    }
}
