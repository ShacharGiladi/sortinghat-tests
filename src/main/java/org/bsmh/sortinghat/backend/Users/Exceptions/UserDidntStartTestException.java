package org.bsmh.sortinghat.backend.Users.Exceptions;

public class UserDidntStartTestException extends Exception  {
    public UserDidntStartTestException() {
        super("User didnt start the test");
    }
}
